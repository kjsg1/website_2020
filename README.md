# 인사이트온 홈페이지 2020

## 개발환경 세팅

- `nodejs`, `npm` 설치 (`npm` 대신 `yarn`도 가능): 구글링 하는것을 추천함
- `npm` 패키지들 설치

```
npm install
```

```
yarn
```

## Gatsby 시작

```
gatsby develop
```

명령을 실행한 뒤, localhost:8000 에서 확인

## 배포

현재 gitlab의 master branch가 업데이트 되면, netlify 에 자동 배포되고 있음
