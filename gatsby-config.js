module.exports = {
  siteMetadata: {
    title: `인사이트온`,
    headline: `인사이트온 홈페이지`,
    description: `Enabling Smart Factory in a day. Enhance it everyday.`,
    author: `@InsightOn`,
    siteUrl: `https://www.insighton.co.kr`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-sitemap`,
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        host: 'https://www.insighton.co.kr',
        sitemap: 'https://www.insighton.co.kr/sitemap.xml',
        policy: [{ userAgent: '*', allow: '/' }]
      }
    },

  ],
}
