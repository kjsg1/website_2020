import React from "react"

import Layout from "../components/Layout"
import SEO from "../components/SEO"

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not found" />
    <h1 style={{ margin: "auto" }}>페이지가 없습니다!</h1>
  </Layout>
)

export default NotFoundPage
