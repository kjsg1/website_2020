import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import AccomplishmentContainer from "../../components/Accomplishment"

import backgroundImage from "../../images/topimg08.jpg"
import TopImageSection from "../../components/TopImageSection"

const AccomplishmentPage = () => (
  <Layout>
    <SEO title="수행실적/구축사례" />
    <TopImageSection
      backgroundImage={backgroundImage}
      title={<span>수행실적</span>}
      description={``}
    />
    <AccomplishmentContainer />
  </Layout>
)

export default AccomplishmentPage
