import React from "react"

import Layout from "../components/Layout"
import SEO from "../components/SEO"
import MainSlide from "../components/Home/MainSlide"
import WhyInsightOn from "../components/Home/WhyInsightOn"
import BestSolution from "../components/Home/BestSolution"
import Development from "../components/Home/Development"
import NewsPr from "../components/Home/NewsPr"
import Reference from "../components/Home/Reference"
import Support from "../components/Home/Support"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <MainSlide />
    <WhyInsightOn />
    <BestSolution />
    <Development />
    <NewsPr />
    <Reference isInMainPage={true}/>
    <Support />
  </Layout>
)

export default IndexPage
