import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import NewsContainer from "../../components/Company/NewsContainer"

import backgroundImage from "../../images/topimg17.jpg"
import TopImageSection from "../../components/TopImageSection"

const NewsPage = () => (
  <Layout>
    <SEO title="뉴스&PR" />
    <TopImageSection
      backgroundImage={backgroundImage}
      title={
        <span>
          뉴스 <span className="light">&amp;</span> PR
        </span>
      }
      description={``}
    />
    <NewsContainer />
  </Layout>
)

export default NewsPage
