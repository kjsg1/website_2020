import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import CareerContainer from "../../components/Company/CareerContainer"

import backgroundImage from "../../images/topimg22.jpg"
import TopImageSection from "../../components/TopImageSection"

const CareerPage = () => (
  <Layout>
    <SEO title="채용공고" />
    <TopImageSection
      backgroundImage={backgroundImage}
      title={
        <span>
          채용공고
        </span>
      }
      description={``}
    />
    <CareerContainer />
  </Layout>
)

export default CareerPage
