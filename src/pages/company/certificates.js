import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import CertificatesContainer from "../../components/Company/CertificatesContainer"

import backgroundImage from "../../images/topimg16.jpg"
import TopImageSection from "../../components/TopImageSection"

const CertificatesPage = () => (
  <Layout>
    <SEO title="특허 및 인증현황" />
    <TopImageSection
      backgroundImage={backgroundImage}
      title={`특허 및 인증`}
      description={``}
    />
    <CertificatesContainer />
  </Layout>
)

export default CertificatesPage
