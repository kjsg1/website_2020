import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import CompanyContainer from "../../components/Company"

import backgroundImage from "../../images/topimg01.jpg"
import TopImageSection from "../../components/TopImageSection"

const CompanyPage = () => (
  <Layout>
    <SEO title="회사소개" />
    <TopImageSection
      backgroundImage={backgroundImage}
      title={`INSIGHT ON`}
      description={``}
    />
    <CompanyContainer />
  </Layout>
)

export default CompanyPage
