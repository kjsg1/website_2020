import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import InquiryContainer from "../../components/Support/InquiryContainer"
import SimpleInquiry from "../../components/SimpleInquiry"

import backgroundImage from "../../images/topimg20.jpg"
import TopImageSection from "../../components/TopImageSection"

import { useWindowSize } from "../../hooks/use-window-size"

const InquiryPage = () => {
  const size = useWindowSize()

  const isMobileClient = size.width && size.width <= 767

  return (
    <>
      <Layout>
        <SEO title="문의하기" />
        <TopImageSection
          backgroundImage={backgroundImage}
          title={`문의하기`}
          description={``}
        />

        {isMobileClient ? <section className="container"><SimpleInquiry /> </section>: <InquiryContainer />}
      </Layout>
    </>
  )
}

export default InquiryPage
