import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import ContactContainer from "../../components/Support/ContactContainer"

import backgroundImage from "../../images/topimg21.jpg"
import TopImageSection from "../../components/TopImageSection"

const ContactPage = () => (
  <Layout>
    <SEO title="찾아오시는 길" />
    <TopImageSection
      backgroundImage={backgroundImage}
      title={`찾아오시는 길`}
      description={``}
    />
    <ContactContainer />
  </Layout>
)

export default ContactPage
