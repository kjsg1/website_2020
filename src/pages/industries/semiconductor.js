import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import SemiConductorContainer from "../../components/Industry/SemiConductorContainer"

import backgroundImage from "../../images/topimg02.jpg"
import TopImageSection from "../../components/TopImageSection"

const SemiConductorPage = () => (
  <Layout>
    <SEO title="반도체" />
    <TopImageSection
      backgroundImage={backgroundImage}
      title={`반도체`}
      description={`모든 IT 제품의 필수불가결한 핵심 부품!`}
    />
    <SemiConductorContainer />
  </Layout>
)

export default SemiConductorPage
