import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import MaterialContainer from "../../components/Industry/MaterialContainer"

import backgroundImage from "../../images/topimg04.jpg"
import TopImageSection from "../../components/TopImageSection"

const MaterialPage = () => (
  <Layout>
    <SEO title="소재/부품" />
    <TopImageSection
      backgroundImage={backgroundImage}
      title={
        <span>
          소재<span className="light">/</span>부품
        </span>
      }
      description={<span>모든 산업은 소재/부품에서 시작된다!</span>}
    />

    <MaterialContainer />
  </Layout>
)

export default MaterialPage
