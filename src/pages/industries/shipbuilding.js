import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import ShipbuildingContainer from "../../components/Industry/ShipbuildingContainer"

import backgroundImage from "../../images/topimg06.jpg"
import TopImageSection from "../../components/TopImageSection"

const ShipBuildingPage = () => (
  <Layout>
    <SEO title="조선" />
    <TopImageSection
      backgroundImage={backgroundImage}
      title={`조선`}
      description={
        <span>
          한국이 세계 시장을 주도한다! 위기를 넘어 지속가능성을 향하여!
        </span>
      }
    />

    <ShipbuildingContainer />
  </Layout>
)

export default ShipBuildingPage
