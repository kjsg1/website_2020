import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import PetroChemContainer from "../../components/Industry/PetroChemContainer"

import backgroundImage from "../../images/topimg05.jpg"
import TopImageSection from "../../components/TopImageSection"

const PetroChemPage = () => (
  <Layout>
    <SEO title="석유화학" />
    <TopImageSection
      backgroundImage={backgroundImage}
      title={`에너지·화학`}
      description={
        <span>
          국내 제조업의 주력산업!
          <br />
          석유시장의 패러다임 변화를 선도해야 합니다.{" "}
        </span>
      }
    />
    <PetroChemContainer />
  </Layout>
)

export default PetroChemPage
