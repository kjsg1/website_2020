import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import CarBatteryContainer from "../../components/Industry/CarBatteryContainer"

import backgroundImage from "../../images/topimg03.jpg"
import TopImageSection from "../../components/TopImageSection"

const CarBatteryPage = () => (
  <Layout>
    <SEO title="자동차/배터리" />
    <TopImageSection
      backgroundImage={backgroundImage}
      title={
        <span>
          자동차<span className="light">/</span>배터리
        </span>
      }
      description={
        <span>
          글로벌 자동차산업 패러다임의 전환!
          <br />
          종합 모빌리티 산업, 친환경 동력원 전기차로의 중심 이동
        </span>
      }
    />
    <CarBatteryContainer />
  </Layout>
)

export default CarBatteryPage
