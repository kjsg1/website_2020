import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import MethodologyContainer from "../../components/Solution/MethodologyContainer"

import backgroundImage from "../../images/topimg14.jpg"
import TopImageSection from "../../components/TopImageSection"

const MethodologyPage = () => (
  <Layout>
    <SEO title="스마트공장 개발방법론" />
    <TopImageSection
      backgroundImage={backgroundImage}
      title={`스마트공장 개발방법론`}
      description={`Smart Factory 모델링 방법론(Object State and Activity Modeling)과 애자일 SW 개발방법론의 통합`}
    />

    <MethodologyContainer />
  </Layout>
)

export default MethodologyPage
