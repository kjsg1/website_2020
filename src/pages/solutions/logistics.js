import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import LogisticsContainer from "../../components/Solution/LogisticsContainer"

import backgroundImage from "../../images/topimg10.jpg"
import TopImageSection from "../../components/TopImageSection"

const LogisticsPage = () => (
  <Layout>
    <SEO title="물류관리" />
    <TopImageSection
      backgroundImage={backgroundImage}
      title={`물류관리`}
      description={`생산활동을 위해 필요한 원재료, 반제품, 제품 등의 최적 보유량과 물류센터 프로세스의 통합관리`}
    />

    <LogisticsContainer />
  </Layout>
)

export default LogisticsPage
