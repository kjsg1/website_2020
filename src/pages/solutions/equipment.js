import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import EquipmentContainer from "../../components/Solution/EquipmentContainer"

import backgroundImage from "../../images/topimg18.jpg"
import TopImageSection from "../../components/TopImageSection"

const EquipmentPage = () => (
  <Layout>
    <SEO title="설비관리" />
    <TopImageSection
      backgroundImage={backgroundImage}
      title={`설비관리`}
      description={`시스템 기반의 설비관리와 모니터링`}
    />

    <EquipmentContainer />
  </Layout>
)

export default EquipmentPage
