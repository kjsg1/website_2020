import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import OperationContainer from "../../components/Solution/OperationContainer"

import backgroundImage from "../../images/topimg09.jpg"
import TopImageSection from "../../components/TopImageSection"

const OperationPage = () => {
  return (
    <Layout>
      <SEO title="생산관리" />
      <TopImageSection
        backgroundImage={backgroundImage}
        title={`생산관리`}
        description={`수주에서 완제품 출하에 이르는 전체 생산활동의 통합 관리`}
      />
      <OperationContainer />
    </Layout>
  )
}

export default OperationPage
