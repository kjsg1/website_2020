import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import AutomationContainer from "../../components/Solution/AutomationContainer"

import backgroundImage from "../../images/topimg19.jpg"
import TopImageSection from "../../components/TopImageSection"

const AutomationPage = () => (
  <Layout>
    <SEO title="설비자동화" />
    <TopImageSection
      backgroundImage={backgroundImage}
      title={`설비자동화`}
      description={`제조자동화를 위한 필수 요건`}
    />

    <AutomationContainer />
  </Layout>
)

export default AutomationPage
