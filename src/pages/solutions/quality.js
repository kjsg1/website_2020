import React from "react"

import Layout from "../../components/Layout"
import SEO from "../../components/SEO"
import QualityContainer from "../../components/Solution/QualityContainer"

import backgroundImage from "../../images/topimg11.jpg"
import TopImageSection from "../../components/TopImageSection"

const QualityPage = () => (
  <Layout>
    <SEO title="품질관리" />
    <TopImageSection
      backgroundImage={backgroundImage}
      title={`품질관리`}
      description={`대량생산 및 초정밀 생산환경에서 기업의 이익과 직결되는 또 하나의 중요요소!`}
    />
    <QualityContainer />
  </Layout>
)

export default QualityPage
