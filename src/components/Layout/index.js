import React, {useEffect} from "react"
import PropTypes from "prop-types"

import Header from "./Header"
import Footer from "./Footer"

import useLocalStorage from '../../hooks/use-local-storage'


const Layout = ({ children }) => {
  const [layoutStyle, _] = useLocalStorage("layout-style")

  useEffect(() => {
    const viewport = document.querySelector('meta[name="viewport"]')
    if (layoutStyle === "pc"){
      viewport.setAttribute("content", "width=1024, initial-scale=0, shrink-to-fit=no")
    }
    if (layoutStyle === "mobile"){
      viewport.setAttribute("content", "width=device-width, initial-scale=1, shrink-to-fit=no")
    }
  })
  
  return (
    <>
      <Header />
      <div>
        <main>{children}</main>
      </div>
      <Footer />
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
