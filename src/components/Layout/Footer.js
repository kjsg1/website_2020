import React from "react"
import { Link } from "gatsby"

import "./footer.css"

import logob from "../../images/logob.jpg"
import useLocalStorage from "../../hooks/use-local-storage"

const Footer = () => {
  const [layoutStyle, setLayoutStyle] = useLocalStorage("layout-style")

  return (
    <div className="footer-container">
      <div className="footmenubox">
        <div className="footermenu1">
          <ul>
            <li className="title">산업분야</li>
            <li>
              <Link to="/industries/semiconductor">반도체</Link>
            </li>
            <li>
              <Link to="/industries/car-battery">자동차/배터리</Link>
            </li>
            <li>
              <Link to="/industries/material">소재/부품</Link>
            </li>
            <li>
              <Link to="/industries/petro-chem">에너지·화학</Link>
            </li>
            <li>
              <Link to="/industries/shipbuilding">조선</Link>
            </li>
          </ul>
        </div>
        <div className="footermenu">
          <ul>
            <li className="title">솔루션</li>
            <li>
              <Link to="/solutions/operation">생산관리</Link>
            </li>
            <li>
              <Link to="/solutions/logistics">물류관리</Link>
            </li>
            <li>
              <Link to="/solutions/quality">품질관리</Link>
            </li>
            <li>
              <Link to="/solutions/equipment">설비관리</Link>
            </li>
            <li>
              <Link to="/solutions/automation">설비자동화</Link>
            </li>
            <li>
              <Link to="/solutions/methodology">스마트공장 개발방법론</Link>
            </li>
          </ul>
        </div>
        <div className="footermenu">
          <ul>
            <li className="title">수행실적</li>
            <li>
              <Link to="/accomplishments">수행실적</Link>
            </li>
          </ul>
        </div>
        <div className="footermenu">
          <ul>
            <li className="title">회사소개</li>
            <li>
              <Link to="/company">INSIGHT ON</Link>
            </li>
            <li>
              <Link to="/company/certificates">특허 및 인증</Link>
            </li>
            <li>
              <Link to="/company/news">뉴스 &amp; PR</Link>
            </li>
            <li>
              <Link to="/company/career">채용공고</Link>
            </li>
          </ul>
        </div>
        <div className="footermenu">
          <ul>
            <li className="title">고객지원</li>
            <li>
              <Link to="/support/inquiry">문의하기</Link>
            </li>
            <li>
              <Link to="/support/contact">찾아오시는 길</Link>
            </li>
          </ul>
        </div>
      </div>

      <div className="footer-line"> </div>

      <div className="footer">
        <address>
          <span className="light">(본사)</span> 울산 중구 종가로 15,
          울산테크노파크 기술혁신B동 402호/404호 |{" "}
          <span className="light">(여수)</span> 여수 삼동3길 17 전남여수산학융합원 301호
          <br />
          <span className="light">(서울)</span> 서울 강남구 강남대로 132길4, 2층 (논현동, 진빌딩)
          <br />
          대표번호: 052-248-5188
          <br />
          Copyrightⓒ Insight On. All Rights Reserved.
          <br />
          <div>
            <img src={logob} alt="인사이트온 로고" />
          </div>
          
          {
              (layoutStyle === "pc") ? 
                (<div
                  className="link_m"
                  onClick={() => {
                    setLayoutStyle("mobile")
                    window.location.reload()
                  }}
                >모바일 버전</div>) : null  
            }
          
        </address>
      </div>

      <div className="m_footer">
        (본사) 울산 중구 종가로 15, 울산테크노파크 기술혁신B동 402호/404호 | (여수) 여수 삼동3길 17 전남여수산학융합원 301호
        <br/>
        (서울) 서울 강남대로 132길4 (논현동, 진빌딩)
        <br />
        대표번호: 052-248-5188
        <br />
        Copyrightⓒ Insight On. All Rights Reserved.
        <div className="footerlogo">
          <img src={logob} alt="인사이트온 로고" />
          <div
            className="link_m"
            onClick={() => {
              setLayoutStyle("pc")
              window.location.reload();
            }}
          >
            PC 버전
          </div>
        </div>
      </div>
    </div>
  )
}

export default Footer
