import { Link } from "gatsby"
import React from "react"

import "./header.css"
import logo from "../../images/logo.png"
import logosmall from "../../images/logosmall.png"
import iconblog from "../../images/m_iconright.png"
import iconfreemon from "../../images/m_iconleft.png"
import iconhiclass from "../../images/m_iconcenter.png"

const Header = () => {
  return (
    <header className="header">
      <div className="header-logo">
        <Link to="/">
          <img src={logo} alt="로고" />
        </Link>
      </div>

      <nav className="dropdown-menu">
        <div className="m_dropdown-menu">menu</div>
        <div className="menu-items">
          <div className="menu-item">
            <Link to="/industries/semiconductor">산업분야</Link>
          </div>
          <div className="menu-item">
            <Link to="/solutions/operation">솔루션</Link>
          </div>
          <div className="menu-item">
            <Link to="/accomplishments">수행실적</Link>
          </div>
          <div className="menu-item">
            <Link to="/company">회사소개</Link>
          </div>
          <div className="menu-item">
            <Link to="/support/inquiry">고객지원</Link>
          </div>
          <div className="header-icon">
            <div>
              <a href="https://blog.naver.com/insight_on" title="블로그" target="_blank">
                <img src={iconblog} alt="블로그" />
              </a>
            </div>
            <div>
              <a href="https://freemon.co.kr" title="프리몬" target="_blank">
                <img src={iconfreemon} alt="freemon" />
              </a>
            </div>
            <div>
              <a href="https://hiclass.zone" title="하이클래스" target="_blank">
                <img src={iconhiclass} alt="hiclass" />
              </a>
            </div>
          </div>
        </div>

        <div className="dropdown-menu-content">
          <div className="dropdown-menu-box">
            <div className="m_header-logosmall">
              <Link to="/">
                <img src={logosmall} alt="로고" />
              </Link>
            </div>

            <div className="dropdown-menu-box-content">
              <ul>
                <li className="dropmenutitle">산업분야</li>
                <div>
                  <li>
                    <Link to="/industries/semiconductor">반도체</Link>
                  </li>
                  <li>
                    <Link to="/industries/car-battery">자동차/배터리</Link>
                  </li>
                  <li>
                    <Link to="/industries/material">소재/부품</Link>
                  </li>
                  <li>
                    <Link to="/industries/petro-chem">에너지·화학</Link>
                  </li>
                  <li>
                    <Link to="/industries/shipbuilding">조선</Link>
                  </li>
                </div>
              </ul>
            </div>
            <div className="dropdown-menu-box-content">
              <ul>
                <li className="dropmenutitle">솔루션</li>
                <div>
                  <li>
                    <Link to="/solutions/operation">생산관리</Link>
                  </li>
                  <li>
                    <Link to="/solutions/logistics">물류관리</Link>
                  </li>
                  <li>
                    <Link to="/solutions/quality">품질관리</Link>
                  </li>
                  <li>
                    <Link to="/solutions/equipment">설비관리</Link>
                  </li>
                  <li>
                    <Link to="/solutions/automation">설비자동화</Link>
                  </li>
                  <li>
                    <Link to="/solutions/methodology">
                      스마트공장 개발방법론
                    </Link>
                  </li>
                </div>
              </ul>
            </div>
            <div className="dropdown-menu-box-content">
              <ul>
                <li className="dropmenutitle">수행실적</li>
                <div>
                  <li>
                    <Link to="/accomplishments">수행실적</Link>
                  </li>
                </div>
              </ul>
            </div>
            <div className="dropdown-menu-box-content">
              <ul>
                <li className="dropmenutitle">회사소개</li>
                <div>
                  <li>
                    <Link to="/company">INSIGHT ON</Link>
                  </li>
                  <li>
                    <Link to="/company/certificates">특허 및 인증</Link>
                  </li>
                  <li>
                    <Link to="/company/news">뉴스 &amp; PR</Link>
                  </li>
                  <li>
                    <Link to="/company/career">채용공고</Link>
                  </li>
                </div>
              </ul>
            </div>
            <div className="dropdown-menu-box-content">
              <ul>
                <li className="dropmenutitle">고객지원</li>
                <div>
                  <li>
                    <Link to="/support/inquiry">문의하기</Link>
                  </li>
                  <li>
                    <Link to="/support/contact">찾아오시는 길</Link>
                  </li>
                </div>
              </ul>
            </div>
            <div className="m_header-icon">
              <div>
                <a href="https://freemon.co.kr" title="프리몬" target="_blank">
                  <img src={iconfreemon} alt="freemon" />
                </a>
                <p>프리몬</p>
              </div>
              <div>
                <a href="https://hiclass.zone" title="하이클래스" target="_blank">
                  <img src={iconhiclass} alt="hiclass" />
                </a>
                <p>하이클래스</p>
              </div>
              <div>
                <a href="https://blog.naver.com/insight_on" title="블로그" target="_blank">
                  <img src={iconblog} alt="블로그" />
                </a>
                <p>블로그</p>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </header>
  )
}

export default Header
