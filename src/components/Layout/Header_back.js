import { Link } from "gatsby"
import React from "react"

import logo from "../images/logo.png"
import menuline from "../images/menuline.jpg"

const Header = () => (
  <header>
    <div className="menuwidth">
      <div className="home">
        <Link to="/">
          <img src={logo} alt="로고" />
        </Link>
      </div>
      <nav className="dropdown-menu">
        <ul>
          <li>
            <Link to="/industries/semiconductor">산업분야</Link>
          </li>
          <li>
            <Link to="/solutions/operation">솔루션</Link>
          </li>
          <li>
            <Link to="/accomplishments">수행실적/구축사례</Link>
          </li>
          <li>
            <Link to="/company">회사소개</Link>
          </li>
          <li>
            <Link to="/support/inquiry">고객지원</Link>
          </li>
        </ul>
      </nav>
    </div>
    <div className="dropdown-menu-content">
      <div className="dropdown-menu-box">
        <div className="dropdown-menu-content1">
          <ul>
            <li>
              <img src={menuline} alt="라인" />
            </li>
            <li>
              <Link to="/industries/semiconductor">반도체</Link>
            </li>
            <li>
              <Link to="/industries/car-battery">자동차/배터리</Link>
            </li>
            <li>
              <Link to="/industries/material">소재/부품</Link>
            </li>
            <li>
              <Link to="/industries/petro-chem">석유화학</Link>
            </li>
            <li>
              <Link to="/industries/shipbuilding">조선</Link>
            </li>
          </ul>
        </div>
        <div className="dropdown-menu-content1">
          <ul>
            <li>
              <img src={menuline} alt="라인" />
            </li>
            <li>
              <Link to="/solutions/operation">생산관리</Link>
            </li>
            <li>
              <Link to="/solutions/logistics">물류관리</Link>
            </li>
            <li>
              <Link to="/solutions/quality">품질관리</Link>
            </li>
            <li>
              <Link to="/solutions/equipment">설비관리</Link>
            </li>
            <li>
              <Link to="/solutions/automation">설비자동화</Link>
            </li>
            <li>
              <Link to="/solutions/methodology">스마트공장 개발방법론</Link>
            </li>
            <li>
              <Link to="/solutions/security">산업보안</Link>
            </li>
          </ul>
        </div>
        <div className="dropdown-menu-content1">
          <ul>
            <li>
              <img src={menuline} alt="라인" />
            </li>
            <li>
              <Link to="/accomplishments">분야별</Link>
            </li>
            <li>
              <Link to="/accomplishments">년도별</Link>
            </li>
          </ul>
        </div>
        <div className="dropdown-menu-content1">
          <ul>
            <li>
              <img src={menuline} alt="라인" />
            </li>
            <li>
              <Link to="/company">INSIGHT ON</Link>
            </li>
            <li>
              <Link to="/company/certificates">특허 및 인증현황</Link>
            </li>
            <li>
              <Link to="/company/news">뉴스&PR</Link>
            </li>
          </ul>
        </div>
        <div className="dropdown-menu-content1">
          <ul>
            <li>
              <img src={menuline} alt="라인" />
            </li>
            <li>
              <Link to="/support/inquiry">문의하기</Link>
            </li>
            <li>
              <Link to="/support/contact">찾아오시는 길</Link>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </header>
)

export default Header
