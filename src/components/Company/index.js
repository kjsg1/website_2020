import React from "react"

import "./indexContainer.css"

import VerticalLine from "../VerticalLine"
import HorizontalLine from "../HorizontalLine"
import HorizontalBottomLine from "../HorizontalBottomLine"
import SimpleInquiry from "../SimpleInquiry"

import InsightOnSpecial from "./InsightOnSpecial"
import History from "./History"
import Organization from "./Organization"

const CompanyContainer = () => (
  <>
    <VerticalLine />

    <section className="company-info container">
      <h3 className="company-info-title">
        " Enabling Smart Factory in a day. Enhance it everyday "
      </h3>
      <div className="company-info-text-wrap">
        <p className="company-info-text">
          인사이트온은 스마트팩토리 전문 IT 서비스 기업으로, 제조업의 생산,
          물류, 품질관리 시스템 등 다양한 시스템 구축 경험과 노하우를 보유한
          회사입니다.
        </p>
        <p className="company-info-text">
          다양한 경험을 갖춘 구성원들의 통찰력으로, 현장의 문제를 해결하는
          솔루션을 개발했으며, 이를 현장에 적용하는 인사이트만의 개발방법론을
          가지고 있습니다.
        </p>
        <p className="company-info-text">
          혁신적인 IT기술과 다양한 경험을 토대로 더 나은 미래를 위한 성장과
          발전에 또 한 번의 혁신을 위해 한 걸음 더 나아갈 것입니다.
        </p>
      </div>
    </section>

    <HorizontalLine />

    <div className={`section container insighton-special-section`}>
      <InsightOnSpecial />
    </div>

    <VerticalLine />

    <History />

    <HorizontalLine />

    <Organization />

    <VerticalLine />

    <SimpleInquiry />

    <HorizontalBottomLine />
  </>
)

export default CompanyContainer
