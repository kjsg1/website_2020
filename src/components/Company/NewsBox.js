import React from "react"

import "./newsBox.css"

const NewsBox = ({ img, type, title, date, link }) => (
  <div className="news-box">
    <div className="news-box-tag">
      <div>{type}</div>
    </div>
    <div className="news-box-image-container">
      <div className="news-box-image-wrapper">
        <img alt="기사사진" src={img} />
      </div>
    </div>
    <div className="news-box-text-wrapper">
      <a href={"//" + link} target="_blank" rel="noreferrer">
        <p>{title}</p>
        <div className="news-box-bottom">
          <div className="news-box-date">{date}</div>

          <div className="news-box-linkbutton"></div>
        </div>
      </a>
    </div>
  </div>
)

export default NewsBox
