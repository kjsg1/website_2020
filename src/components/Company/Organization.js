import React from "react"

import "./organization.css"

const Organization = () => (
  <>
    <section className="or_chart">
      <h3 className="title_h3">조직도</h3>
      <div className="wrap">
        <div className="system">
          <ul className="systemlist">
            <li>
              <p>대표이사</p>
            </li>
            <li className="in01">경영지원</li>
            <li className="in02">제조컨설팅</li>
            <li className="in03">SI사업팀</li>
            <li className="in04">SM사업팀</li>
            <li className="in05">플랫폼사업팀</li>
            <li className="in06">부설연구소</li>
            <li className="in07">영업기획</li>
          </ul>
        </div>
      </div>
    </section>
  </>
)

export default Organization
