import React from "react"

import VerticalLine from "../VerticalLine"
import HorizontalBottomLine from "../HorizontalBottomLine"

import news_20210329_1 from "../../images/news_20210329-1.jpg"
import news01 from "../../images/news_con01.jpg"
import news02 from "../../images/news_con02.jpg"
import news20200203 from "../../images/news-20200203.jpg"
import news20200128 from "../../images/news-20200128.jpg"
import news20191111 from "../../images/news-20191111.png"
import news20191023 from "../../images/news-20191023.png"
import news20191012 from "../../images/news-20191012.jpg"
import news_201125 from "../../images/news_201125.jpg"
import news202009 from "../../images/news-202009.jpg"
import news_20220722 from "../../images/news_20220722.jpg"

import "./newsContainer.css"
import NewsBox from "./NewsBox"

const NewsContainer = () => (
  <>
    <VerticalLine />
    <section className="container">
      <div className="news-section-description">
        인사이트온의 새로운 소식과 다양한 활동들을 빠르고 정확하게 전해드립니다.
      </div>

      <div className="news-boxes-container">
        <NewsBox
          img={news_20220722}
          type="기사"
          title="2022년 울산 스타기업 지정"
          date="2022.07.22"
          link="m.etnews.com/20220722000053?obj=Tzo4OiJzdGRDbGFzcyI6Mjp7czo3OiJyZWZlcmVyIjtOO3M6NzoiZm9yd2FyZCI7czoxMzoid2ViIHRvIG1vYmlsZSI7fQ%3D%3D"
        />
        <NewsBox
          img={news_20210329_1}
          type="기사"
          title="울산 중구, 4차산업기반 IT기업 대표 간담회 초청"
          date="2021.03.26"
          link="ulsanpress.net/news/articleView.html?idxno=373972"
        />

        <NewsBox
          img={news_201125}
          type="홍보"
          title="2020 울산 스타트업 TOP10 선정"
          date="2020.11.25"
          link="blog.naver.com/insight_on/222154227032"
        />
        <NewsBox
          img={news202009}
          type="홍보"
          title="2020 글로벌 ICT 유망기업 선정"
          date="2020.09.07"
          link="blog.naver.com/insight_on/222082760694"
        />
        <NewsBox
          img={news20200203}
          type="기사"
          title="SW개발자·SI프로젝트 연결 '프리몬'…매칭 상담액 400억 돌파"
          date="2020.02.03"
          link="blog.naver.com/insight_on/221796035910"
        />
        <NewsBox
          img={news20200128}
          type="방송"
          title="울산 KBS 보물창고 당신의 꿈 [인사이트온]"
          date="2020.01.28"
          link="blog.naver.com/insight_on/221795834115"
        />
        <NewsBox
          img={news02}
          type="기사"
          title="[2019 하반기 대한민국 우수특허대상] 인사이트온"
          date="2019.12.19"
          link="blog.naver.com/freemon1/221735014338"
        />
        <NewsBox
          img={news01}
          type="기사"
          title="기업의 인재고용과 프리랜서의 일자리 확보라는 두 마리 토끼를 잡은 사람냄새 나는 플랫폼 '프리몬'"
          date="2019.11.20"
          link="www.sisanewstime.co.kr/news/articleView.html?idxno=6168"
        />
        <NewsBox
          img={news20191111}
          type="기사"
          title="SW 개발자·SI 프로젝트 실시간 매칭…인사이트온 '프리몬' 화제"
          date="2019.11.11"
          link="www.etnews.com/20191111000228"
        />
        <NewsBox
          img={news20191023}
          type="기사"
          title="[울산, 창업 성공신화 꿈꾸다] 제조업 밀집 울산서 스마트공장 도약 발판"
          date="2019.10.23"
          link="blog.naver.com/insight_on/221688612239"
        />
        <NewsBox
          img={news20191012}
          type="홍보"
          title="울산중기청, 유망IT기업 (주)인사이트온 기업방문"
          date="2019.10.12"
          link="blog.naver.com/ulsanmss/221376267970"
        />
        {/*<div className="news-box mobile-hidden"></div>*/}
      </div>
    </section>

    <HorizontalBottomLine />
  </>
)

export default NewsContainer
