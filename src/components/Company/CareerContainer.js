import React from "react"

import VerticalLine from "../VerticalLine"
import HorizontalBottomLine from "../HorizontalBottomLine"


import "./careerContainer.css"

const CareerContainer = () => (
  <>
    <VerticalLine />
    <section className="container">
    <h3 className="company-career-title">모집분야</h3>
      <div className="career-boxes-container">
        <div className="career-box">
          <h4>Project Manager</h4>
          <ul className="career-list">
            <li>서비스 기획업무</li>
            <li>프로젝트 제안 업무</li>
            <li>프로젝트 관리 업무</li>
          </ul>
          <div className="career-box-bottom">
            <h5>필수 사항</h5>
            <p>IT 경력 10년 이상</p>
          </div>
          <div className="career-box-bottom">
            <h5>우대 사항</h5>
            <p>제조 영역 업무 이해도</p>
            <p>SI 프로젝트 PM 유경험자</p>
          </div>
        </div>
        <div className="career-box">
          <h4>Project Leader</h4>
          <ul className="career-list">
            <li>프로젝트 리딩
              <ul>
                <li>1) MES/WMS</li>
                <li>2) EMS(설비관리)</li>
                <li>3) LIMS/QMS</li>
                <li>4) 기타 제조 시스템</li>
              </ul>
            </li>
            <li>분석/설계</li>
          </ul>
          <div className="career-box-bottom">
            <h5>필수 사항</h5>
            <p>IT 경력 10년 이상</p>
          </div>
          <div className="career-box-bottom">
            <h5>우대 사항</h5>
            <p>제조 영역 업무 이해도</p>
            <p>SI 프로젝트 PL 유경험자</p>
          </div>
        </div>
        <div className="career-box">
          <h4>Web Development</h4>
          <ul className="career-list">
            <li>Java</li>
            <li>ASP.NET(C#, VB)</li>
            <li>WPF</li>
          </ul>
          <div className="career-box-bottom">
            <h5>필수 사항</h5>
            <p>웹 개발 가능</p>
          </div>
          <div className="career-box-bottom">
            <h5>우대 사항</h5>
            <p>클라우드 개발 유경험자</p>
            <p>RDBMS 이해도</p>
            <p>제조 영역 업무 이해도</p>
          </div>
        </div>
        <div className="career-box">
          <h4>Application Development</h4>
          <ul className="career-list">
            <li>C#.NET, VB.NET</li>
            <li>C, C++</li>
          </ul>
          <div className="career-box-bottom">
            <h5>필수 사항</h5>
            <p>응용프로그램 개발 가능</p>
          </div>
          <div className="career-box-bottom">
            <h5>우대 사항</h5>
            <p>RDBMS 이해도</p>
            <p>제조 영역 업무 이해도</p>
          </div>
        </div>
        <div className="career-box">
          <h4>Mobile/Embedded Development</h4>
          <ul className="career-list">
            <li>Android/iOS 앱 개발</li>
            <li>Windows Tablet 앱 개발</li>
            <li>PDA 프로그램 개발</li>
            <li>Kiosk 앱 개발</li>
          </ul>
          <div className="career-box-bottom">
            <h5>필수 사항</h5>
            <p>앱 개발 가능</p>
          </div>
          <div className="career-box-bottom">
            <h5>우대 사항</h5>
            <p>바코드 개발 유경험자</p>
            <p>RDBMS 이해도</p>
          </div>
        </div>
      </div>
    </section>

    <HorizontalBottomLine />
    <section className="container">
    <h3 className="company-career-title">모집안내</h3>
      <div className="career-section-description">
        <h4>근무위치</h4>
        <ul className="career-section-list">
          <li>서울</li>
          <li>대전</li>
          <li>울산</li>
          <li>여수</li>
        </ul>
      </div>
      <div className="career-section-description">
        <h4>지원방법</h4>
        <p>이력서 이메일 접수(<a href="mailto:suhkim98@insighton.kr">suhkim98@insighton.kr</a>)</p>
        <p className="foot-note">* 경영지원팀 김수현 수석 / 연락처 010-9300-2951</p>
      </div>
      <div className="career-section-description">
        <h4>전형절차</h4>
        <p>1차 서류전형 > 2차 인터뷰 > 최종합격</p>
      </div>
    </section>

    <HorizontalBottomLine />
  </>
)

export default CareerContainer
