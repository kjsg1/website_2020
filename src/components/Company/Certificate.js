import React from "react"

import "./certificate.css"

const Certificate = ({ img, type, year, description, highlight }) => (
  <div className="certificate-box">
    <div>
      <img alt="특허인증이미지" src={img} />
    </div>
    <div
      className="certificate-smallbox"
      style={{ backgroundColor: highlight ? "#ff0000" : "cdd4e0" }}
    >
      <div className="certificate-smallbox-title">{type}</div>
      <div className="certificate-smallbox-year">{year}</div>
    </div>
    <div className="certificate-description">{description}</div>
  </div>
)

export default Certificate
