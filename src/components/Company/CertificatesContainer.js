import React from "react"

import "./certificatesContainer.css"

import VerticalLine from "../VerticalLine"
import VerticalBottomLine from "../VerticalBottomLine"
import HorizontalLine from "../HorizontalLine"
import SimpleInquiry from "../SimpleInquiry"
import certificates_img01 from "../../images/certificates_img01.jpg"
import certificates_img02 from "../../images/certificates_img02.jpg"
import certificates_img03 from "../../images/certificates_img03.jpg"
import certificates_img04 from "../../images/certificates_img04.jpg"
import certificates_img05 from "../../images/certificates_img05.jpg"
import certificates_img06 from "../../images/certificates_img06.jpg"
import certificates_img07 from "../../images/certificates_img07.jpg"
import certificates_img08 from "../../images/certificates_img08.jpg"
import certificates_img09 from "../../images/certificates_img09.jpg"
import certificates_img10 from "../../images/certificates_img10.jpg"
import certificates_img11 from "../../images/certificates_img11.jpg"
import certificates_img12 from "../../images/certificates_img12.jpg"
import certificates_img13 from "../../images/certificates_img13.jpg"
import certificates_img14 from "../../images/certificates_img14.jpg"
import certificates_img15 from "../../images/certificates_img15.jpg"
import certificates_img16 from "../../images/certificates_img16.jpg"
import certificates_img17 from "../../images/certificates_img17.jpg"
import awards_201125 from "../../images/awards_201125.jpg"
import awards_201125_1 from "../../images/awards_201125-1.jpg"
import certificates_img20 from "../../images/certificates_img20.jpg"
import certificates_img21 from "../../images/certificates_img21.jpg"
import certificates_img22 from "../../images/certificates_img22.jpg"
import certificates_img23 from "../../images/certificates_img23.jpg"
import certificates_img24 from "../../images/certificates_img24.jpg"
import certificates_img25 from "../../images/certificates_img25.jpg"
import certificates_img200424 from "../../images/certificates_img200424.jpg"
import certificate_innobiz from "../../images/certificate_innobiz.png"
import certificate_pct from "../../images/certificate_pct.png"
import news202009 from "../../images/news-202009.jpg"

import certificates_20220722 from '../../images/certificates_20220722.jpg';
import awards_20220722 from '../../images/awards_20220722.jpg';

import m_icon_certificates01 from "../../images/m_icon_certificates01.png"
import m_icon_certificates02 from "../../images/m_icon_certificates02.png"
import m_icon_certificates03 from "../../images/m_icon_certificates03.png"
import m_icon_certificates04 from "../../images/m_icon_certificates04.png"
import m_icon_certificates05 from "../../images/m_icon_certificates05.png"
import m_icon_certificates06 from "../../images/m_icon_certificates06.png"
import m_icon_certificates07 from "../../images/m_icon_certificates07.png"

import Certificate from "./Certificate"

import "./certificatesContainer.css"

const CertificatesContainer = () => (
  <>
    <VerticalLine />

    <section className="certificates-section">
      <div className="certificates-section-title">인증서</div>
      <div className="certificates-section-row">
        <Certificate
          img={certificates_20220722}
          type="인증"
          description="기술평가 우수기업 인증서"
          highlight={false}
        />

        <Certificate
          img={certificates_img21}
          type="인증"
          description="중소기업확인서"
          highlight={false}
        />
        <Certificate
          img={certificates_img10}
          type="인증"
          description="벤쳐기업확인서"
          highlight={false}
        />
        <Certificate
          img={certificates_img09}
          type="인증"
          description={<span>기업부설연구소 인정서</span>}
          highlight={false}
        />
        <Certificate
          img={certificate_innobiz}
          type="인증"
          description={<span>기술혁신형 중소기업(이노비즈) 확인서</span>}
          highlight={false}
        />
      </div>
    </section>

    <HorizontalLine />

    <section className="certificates-section">
      <div className="certificates-section-title">특허</div>
      <div className="certificates-section-row">
        <Certificate
          img={certificates_img01}
          type="특허증"
          year="2019"
          description={<span>기업과 프리랜서 매칭 방법 및 시스템</span>}
          highlight={true}
        />
        <Certificate
          img={certificate_pct}
          type="PCT출원"
          year="2020"
          description={<span>기업과 프리랜서 매칭 방법 및 시스템</span>}
          highlight={true}
        />
        <Certificate
          img={certificates_img22}
          type="특허출원"
          year="2018"
          description="MES API 자동화 방법"
          highlight={false}
        />
        <Certificate
          img={certificates_img23}
          type="특허출원"
          year="2018"
          description="필름생산 결함 원인공정 추적방법 및 장치"
          highlight={false}
        />
        <Certificate
          img={certificates_img24}
          type="특허출원"
          year="2018"
          description="설비통신 자동화를 위한 통신 시나리오 및 메모리 결합방안 및 장치"
          highlight={false}
        />
        <Certificate
          img={certificates_img02}
          type="특허출원"
          year="2018"
          description={<span>재공품재고조사방법 및 장치</span>}
          highlight={false}
        />
        <Certificate
          img={certificates_img03}
          type="특허출원"
          year="2020"
          description={<span>설비통신 자동화 방법</span>}
          highlight={true}
        />
        <Certificate
          img={certificates_img04}
          type="특허출원"
          year="2020"
          description={<span>제조실행시스템 설계를 위한 방법 및 시스템</span>}
          highlight={true}
        />
        <Certificate
          img={certificates_img200424}
          type="특허출원"
          year="2020"
          description={
            <span>
              입력 순서, 입력 방향 및 입력 주체에 기초한 사용자 입력 인증 방법
              및 시스템
            </span>
          }
          highlight={true}
        />
        <Certificate
          img={certificates_img05}
          type="디자인등록"
          year="2020"
          description={
            <span>기업과 프리랜서 매칭을 위한 프리랜서 검색 디자인</span>
          }
          highlight={false}
        />
        <Certificate
          img={certificates_img06}
          type="디자인등록"
          year="2019"
          description={
            <span>기업과 프리랜서 매칭을 위한 프로젝트 검색 디자인</span>
          }
          highlight={false}
        />
      </div>
    </section>

    <VerticalLine />

    <section className="certificates-section">
      <div className="certificates-section-title">프로그램 등록</div>
      <div className="certificates-section-row">
        <Certificate
          img={certificates_img14}
          type="저작권"
          year="2018"
          description={<span>NFS Locker</span>}
          highlight={false}
        />
        <Certificate
          img={certificates_img15}
          type="저작권"
          year="2018"
          description="NFS MES Framework"
          highlight={false}
        />
        <Certificate
          img={certificates_img16}
          type="저작권"
          year="2019"
          description="Pbot"
          highlight={false}
        />
      </div>
    </section>

    <HorizontalLine />

    <section className="certificates-section">
      <div className="certificates-section-title">상표 등록</div>
      <div className="certificates-section-row">
        <Certificate
          img={certificates_img11}
          type="상표"
          year="2018"
          description={<span>프리몬</span>}
          highlight={false}
        />
        <Certificate
          img={certificates_img12}
          type="상표"
          year="2020"
          description="원큐"
          highlight={false}
        />
        <Certificate
          img={certificates_img13}
          type="상표"
          year="2020"
          description="랩원큐"
          highlight={false}
        />
      </div>
    </section>

    <VerticalLine />

    <section className="certificates-section">
      <div className="certificates-section-title">소프트웨어 인증</div>
      <div className="certificates-section-row">
        <Certificate
          img={certificates_img07}
          type="GS인증"
          year="2019"
          description={<span>Pbot V1.0</span>}
          highlight={false}
        />
        <Certificate
          img={certificates_img08}
          type="V&V인증"
          year="2019"
          description={<span>Pbot V1.0</span>}
          highlight={false}
        />
      </div>
    </section>

    <HorizontalLine />

    <section className="certificates-section">
      <div className="certificates-section-title">기술자료임치</div>
      <div className="certificates-section-row">
        <Certificate
          img={certificates_img25}
          type="기술자료임치"
          year="2019"
          description={
            <span>룰 기반 스마트 팩토리 설비통신 자동화 방법 및 시스템</span>
          }
          highlight={false}
        />
      </div>
    </section>

    <VerticalLine />

    <section className="certificates-section">
      <div className="certificates-section-title">수상 내역</div>
      <div className="certificates-section-row">
      <Certificate
          img={awards_20220722}
          type={<span>기술평가<br/>우수기업</span>}
          year="2022"
          description="기술평가 우수기업 (T-3)"
          highlight={true}
        />
        <Certificate
          img={awards_201125}
          type={<span>스타트업 TOP10</span>}
          year="2020"
          description="울산 스타트업 TOP10 선정"
          highlight={true}
        />
        <Certificate
          img={awards_201125_1}
          type={
            <span>
              지역
              <br />
              우수기업
            </span>
          }
          year="2020"
          description="울산 지역 우수기업 선정"
          highlight={true}
        />
        <Certificate
          img={news202009}
          type={<span>글로벌 ICT 유망기업</span>}
          year="2020"
          description="2020 글로벌 ICT 유망기업 선정"
          highlight={true}
        />
        <Certificate
          img={certificates_img20}
          type="우수특허대상"
          year="2019"
          description={<span>한국일보 우수특허대상 수상</span>}
          highlight={true}
        />
        <Certificate
          img={certificates_img17}
          type={<span>동반성장상</span>}
          year="2019"
          description="SK 동반성장상 수상"
          highlight={true}
        />
      </div>
    </section>

    <section className="m_certificates-section">
      <div className="m_certificates-box">
        <div className="m_certificates-img">
          <img alt="인증서" src={m_icon_certificates01} />
        </div>
        <div className="m_certificates-title">인증서</div>
        <div className="m_certificates-content">
          <ul>
            <li>기술평가 우수기업 인증서</li>
            <li>중소기업확인서</li>
            <li>벤쳐기업확인서</li>
            <li>기업부설연구소 인정서</li>
            <li>기술혁신형 중소기업(이노비즈) 확인서</li>
          </ul>
        </div>
      </div>
      <div className="m_certificates-box">
        <div className="m_certificates-img">
          <img alt="특허" src={m_icon_certificates02} />
        </div>
        <div className="m_certificates-title">특허</div>
        <div className="m_certificates-content">
          <ul>
            <li>기업과 프리랜서 매칭 방법 및 시스템(2019)</li>
            <li>기업과 프리랜서 매칭 방법 및 시스템: PCT 국제출원(2019)</li>
            <li>MES API 자동화 방법 (2018)</li>
            <li>필름생산 결함 원인공정 추적방법 및 장치 (2018)</li>
            <li>
              설비통신 자동화를 위한 통신 시나리오 및 메모리 결합방안 및 장치
              (2018)
            </li>
            <li>재공품재고조사방법 및 장치 (2018)</li>
            <li>설비통신 자동화 방법 (2020)</li>
            <li>제조실행시스템 설계를 위한 방법 및 시스템 (2020)</li>
            <li>
              입력 순서, 입력 방향 및 입력 주체에 기초한 사용자 입력 인증 방법
              및 시스템 (2020)
            </li>
            <li>기업과 프리랜서 매칭을 위한 프리랜서 검색 디자인 (2019)</li>
            <li>기업과 프리랜서 매칭을 위한 프로젝트 검색 디자인 (2019)</li>
          </ul>
        </div>
      </div>
      <div className="m_certificates-box">
        <div className="m_certificates-img">
          <img alt="프로그램 등록" src={m_icon_certificates03} />
        </div>
        <div className="m_certificates-title">프로그램 등록</div>
        <div className="m_certificates-content">
          <ul>
            <li> NFS Locker </li>
            <li> NFS MES Framework </li>
            <li> Pbot</li>
          </ul>
        </div>
      </div>
      <div className="m_certificates-box">
        <div className="m_certificates-img">
          <img alt="상표등록" src={m_icon_certificates04} />
        </div>
        <div className="m_certificates-title">상표등록</div>
        <div className="m_certificates-content">
          <ul>
            <li>프리몬</li>
            <li>원큐</li>
            <li>랩원큐</li>
          </ul>
        </div>
      </div>
      <div className="m_certificates-box">
        <div className="m_certificates-img">
          <img alt="소프트웨어인증" src={m_icon_certificates05} />
        </div>
        <div className="m_certificates-title">소프트웨어 인증</div>
        <div className="m_certificates-content">
          <ul>
            <li> Pbot V1.0 (GS인증 2019) </li>
            <li> Pbot V1.0 (V&V인증 2019)</li>
          </ul>
        </div>
      </div>
      <div className="m_certificates-box">
        <div className="m_certificates-img">
          <img alt="기술자료임치" src={m_icon_certificates06} />
        </div>
        <div className="m_certificates-title">기술자료임치</div>
        <div className="m_certificates-content">
          <ul>
            <li>
              {" "}
              룰 기반 스마트 팩토리 설비통신 자동화 방법 및 시스템 (2019)
            </li>
          </ul>
        </div>
      </div>
      <div className="m_certificates-box">
        <div className="m_certificates-img">
          <img alt="수상내역" src={m_icon_certificates07} />
        </div>
        <div className="m_certificates-title">수상내역</div>
        <div className="m_certificates-content">
          <ul>
            <li> 기술평가 우수기업 (2022) </li>
            <li> 울산 스타트업 TOP10 선정 (2020)</li>
            <li> 울산 지역 우수기업 선정 (2020) </li>
            <li> 2020 글로벌 ICT 유망기업 선정 (2020) </li>
            <li> 한국일보 우수특허대상 수상 (2019) </li>
            <li> SK 동반성장상 수상 (2019)</li>
          </ul>
        </div>
      </div>
    </section>

    <HorizontalLine />

    <SimpleInquiry />

    <VerticalBottomLine />
  </>
)

export default CertificatesContainer
