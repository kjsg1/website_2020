import React from "react"

import vision01 from "../../images/vision01.png"
import vision02 from "../../images/vision02.png"
import vision03 from "../../images/vision03.png"

import "./insightOnSpecial.css"

const InsightOnSpecial = () => (
  <div className="insighton-special-container">
    <div className="insighton-special-box insighton-special-box-1">
      <div className="insighton-special-box-image">
        <img alt="Insight" src={vision01} />
      </div>
      <div className="insighton-special-box-title">Insight</div>
      <div className="insighton-special-box-description">
        현장에서 남다른 통찰력을
        <br /> 발휘해 문제와 답을 찾아
        <br />
        고객 가치 극대화를
        <br />
        선도합니다.
      </div>
      <div className="m_insighton-special-box-description">
        현장에서 남다른 통찰력을 발휘해 문제와
        <br />
        답을 찾아 고객 가치 극대화를 선도합니다.
      </div>
    </div>
    <div className="insighton-special-box insighton-special-box-2">
      <div className="insighton-special-box-image">
        <img alt="Solution" src={vision02} />
      </div>
      <div className="insighton-special-box-title">Solution</div>
      <div className="insighton-special-box-description">
        인사이트온의 솔루션은 단순한
        <br />
        소프트웨어 제품이 아닙니다.
        <br />
        문제의 해결책입니다.
        <br />
        인사이트온은 지속적인 솔루션
        <br />
        성장을 추구합니다.
      </div>
      <div className="m_insighton-special-box-description">
        인사이트온의 솔루션은 단순한 소프트웨어 제품이 아닙니다. 문제의
        해결책입니다. 인사이트온은 지속적인 솔루션 성장을 추구합니다.
      </div>
    </div>
    <div className="insighton-special-box insighton-special-box-3">
      <div className="insighton-special-box-image">
        <img alt="Methodology" src={vision03} />
      </div>
      <div className="insighton-special-box-title">Methodology</div>
      <div className="insighton-special-box-description">
        구성원들이 체득하고 있는
        <br />
        인사이트온만의 방법론으로
        <br />
        고객 현장에서 필요한
        <br />
        솔루션을 누구보다 빠르게
        <br />
        제대로 구현합니다.
      </div>
      <div className="m_insighton-special-box-description">
        구성원들이 체득하고 있는 인사이트온만의 방법론으로 고객 현장에서 필요한
        솔루션을 누구보다 빠르게 제대로 구현합니다.
      </div>
    </div>
  </div>
)

export default InsightOnSpecial
