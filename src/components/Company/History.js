import React from "react"

import "./history.css"

const History = () => (
  <>
    <section className="company-history">
      <h3 className="company-history-title">연혁</h3>
      <div className="history-map">
        <div className="history-year">
          <h3>2022</h3>
          <ul class="history-content">
            <li>
              <em></em>저작권 육성지정사업 선정
              <p>(울산경제진흥원)</p>
            </li>
            <li>
              <em></em>글로벌 ICT 유망기업 재선정
              <p>(울산정보산업진흥원)</p>
            </li>
            <li>
              <em></em>4차산업기반 맞춤형 중소기업 지원사업 선정
              <p>(울산테크노파크)</p>
            </li>
            <li>
              <em></em>지역기업 비대면 판로지원 사업 선정
              <p>(울산경제진흥원)</p>
            </li>
            <li>
              <em></em>
              LabOneQ V1.0(랩원큐V1.0) 저작권 등록
            </li>
            <li>
              <em></em>
              NICE기술평가 우수기업 인증(T3)
              <p>(나이스평가정보㈜)</p>
            </li>
          </ul>
        </div>
        <div className="history-year">
          <h3>2021</h3>
          <ul class="history-content">
            <li>
              <em></em>대중소 상생형 AI 융합(설비관리) 기획연구 사업자 지정
              <p>(울산정보산업진흥원)</p>
            </li>
            <li>
              <em></em>제조시스템 설계 방법 및 시스템 특허등록
            </li>
            <li>
              <em></em>TDMS 및 CAD 뷰어 제품 총판 계약
            </li>
            <li>
              <em></em>글로벌 ICT 유망기업 선정<p>(울산정보산업진흥원)</p>
            </li>
            <li>
              <em></em>사내 벤처육성 운영기업 선정<p>(창업진흥원)</p>
            </li>
            <li>
              <em></em>빅데이터, AI 자문기업 선정<p>(정보산업진흥원)</p>
            </li>
          </ul>
        </div>
        <div className="history-year">
          <h3>2020</h3>
          <ul class="history-content">
            <li>
              <em></em>스타트업 Top 10 기업 표창<p>(울산중기청장)</p>
            </li>
            <li>
              <em></em>지역 우수기업 표창<p>(울산광역시장)</p>
            </li>
            <li>
              <em></em>서울 사무실 개소<p>(2020.08, 강남구 논현동)</p>
            </li>
            <li>
              <em></em>2020년 글로벌 ICT 유망기업 육성사업 최종 선정
              <p>(울산정보산업진흥원)</p>
            </li>
            <li>
              <em></em>"위변조 방지를 위한 블록체인 기반 시험정보관리 클라우드
              플랫폼", 정부지원사업 선정<p>(중소벤처기업부, 2020.07~2021.06)</p>
            </li>
            <li>
              <em></em>기술혁신형 중소기업(이노비즈) 인증 취득, 이노비즈협회
            </li>
            <li>
              <em></em>"상주근무 프리랜서와 수요기업 간 스케쥴 기반 매칭
              플랫폼", 정부지원사업 선정<p>(울산경제진흥원, 2020.05~2021.03)</p>
            </li>
            <li>
              <em></em>"완전 자동화 된 사람과 컴퓨터 식별방법 및 시스템", 특허
              출원
            </li>
            <li>
              <em></em>SK 우수 Business Partner 선정
            </li>
          </ul>
        </div>
        <div className="history-year">
          <h3>2019</h3>
          <ul class="history-content">
            <li>
              <em></em>"설비통신 자동화 제품", GS 인증 획득
            </li>
            <li>
              <em></em>"2019 대한민국 우수특허 대상" 수상, 한국일보
            </li>
            <li>
              <em></em>전문가 매칭 BM, 디자인 특허 출원 및 플랫폼 론칭
            </li>
          </ul>
        </div>
        <div className="history-year">
          <h3>2018</h3>
          <ul class="history-content">
            <li>
              <em></em>기업부설연구소 설립
            </li>
            <li>
              <em></em>벤처기업 인증
            </li>
            <li>
              <em></em>"Rule 기반 Smart Factory 설비 통신 자동화 플랫폼 개발",
              정부과제 주관기관 선정<p>(중소벤처기업부, 2018.10~2020.01)</p>
            </li>
            <li>
              <em></em>SK 일반 Business Partner 전환
            </li>
            <li>
              <em></em>LG CNS Partner 등록
            </li>
            <li>
              <em></em>결함공정 추적 외 2건 특허출원
            </li>
            <li>
              <em></em>"Smart 재고실사" 특허 및 솔루션 개발
            </li>
          </ul>
        </div>
        <div className="history-year">
          <h3>2017</h3>
          <ul class="history-content">
            <li>
              <em></em>"메타데이터 관리 자동화 솔루션" 개발
            </li>
            <li>
              <em></em>SK 한시업체 등록
            </li>
            <li>
              <em></em>울산 본사 이전
            </li>
          </ul>
        </div>
        <div className="history-year">
          <h3>2016</h3>
          <ul class="history-content line_none">
            <li>
              <em></em>중국, 산업보안 솔루션 공급
            </li>
          </ul>
        </div>
        <div className="history-year">
          <h3>2015</h3>
          <ul class="history-content line_none">
            <li>
              <em></em>(주)인사이트온 법인 설립<p>(2015.06)</p>
            </li>
          </ul>
        </div>
      </div>
    </section>
  </>
)

export default History
