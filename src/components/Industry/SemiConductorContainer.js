import React from "react"

import contimg01 from "../../images/contimg01.jpg"

import "./industry.css"

import VerticalLine from "../VerticalLine"
import HorizontalLine from "../HorizontalLine"
import HorizontalBottomLine from "../HorizontalBottomLine"
import SolutionsTabArea from "../SolutionsTabArea"
import SimpleInquiry from "../SimpleInquiry"
import Reference from "../Reference"

const SemiConductorContainer = () => {
  return (
    <>
      <VerticalLine />

      <section className="industry-section-1">
        <h3 className="industry-section-title">반도체 산업동향</h3>
        <div>
          <div className="industry-section-1-img">
            <img alt="반도체이미지1" src={contimg01} />
          </div>
          <div className="industry-section-1-content">
            산업자동화, IT&amp;통신 및 자동차 산업의 급성장으로 인해 반도체
            시장전망은 매우 밝습니다.
            <br />
            <br />
            크게 시스템(비메모리)반도체와 메모리반도체로 구분되며,
            <br />전 세계적으로{" "}
            <span className="bold">
              시스템반도체의 시장규모는 메모리반도체의 2배
            </span>
            에 달하고 있습니다.
            <br />
            <br />
            국내 반도체산업은 메모리반도체 위주로 발전해 글로벌 메모리 반도체
            시장에서
            <br />
            1위를 달리고 있으며, 향후에는{" "}
            <span className="bold">
              시장성과 수익률이 높은 시스템반도체 육성
            </span>
            을 위해
            <br />
            기업과 정부의 대규모 투자가 예상됩니다.
            <br />
            <br />
            정부는 2030년까지 파운드리(생산) 세계 1위, 팹리스 (설계, 개발)
            시장점유율 10% 달성을
            <br />
            목표로 하고 있습니다.
          </div>
          <div className="m_industry-section-1-content">
            산업자동화, IT&amp;통신 및 자동차 산업의 급성장으로 인해 반도체
            시장전망은 매우 밝습니다.
            <br />
            <br />
            크게 시스템(비메모리)반도체와 메모리반도체로 구분되며, 전 세계적으로{" "}
            <span className="bold">
              시스템반도체의 시장규모는 메모리반도체의 2배
            </span>
            에 달하고 있습니다.
            <br />
            <br />
            국내 반도체산업은 메모리반도체 위주로 발전해 글로벌 메모리 반도체
            시장에서 1위를 달리고 있으며, 향후에는{" "}
            <span className="bold">
              시장성과 수익률이 높은 시스템반도체 육성
            </span>
            을 위해 기업과 정부의 대규모 투자가 예상됩니다.
            <br />
            <br />
            정부는 2030년까지 파운드리(생산) 세계 1위, 팹리스 (설계, 개발)
            시장점유율 10% 달성을 목표로 하고 있습니다.
          </div>
        </div>
      </section>

      <HorizontalLine />

      <section className="industry-section-2">
        <h3 className="industry-section-title">공정 및 시스템 측면의 특징</h3>
        <div className="industry-section-2-content">
          <span className="bold">
            수백 개의 장비와 600여 개에 이르는 공정 스텝
          </span>
          이 존재하며,
          <br />
          동일한 설비를 여러 번 반복하게 되는{" "}
          <span className="bold">사이클링 공정(Re-entrant)</span>을 가지는
          특징이 있습니다.
          <br />
          <br />
          특히 주문형 반도체를 제조하는 공정에서는
          <br />
          수많은 제품이 동시에 같은 설비들을 공유하여 더욱 큰 복잡성을
          초래합니다.
          <br />
          <br />{" "}
          <span className="bold">적절한 웨이퍼 할당과 제품투입량 결정</span>
          이 매우 중요한 의사결정 포인트이며,
          <br />
          생산라인은 웨이퍼(Wafer) 혹은 로트(Lot)들이{" "}
          <span className="bold">유동적이고 연속적으로 흘러가도록 관리</span>
          가 되어져야 합니다.
          <br />
          <br />
          이를 통해 설비의 효율성을 높이고, 제조 사이클 타임을 줄이는 것이
          제조원가 경쟁력에 매우 중요합니다.
        </div>

        <div className="m_industry-section-2-content">
          <span className="bold">
            수백 개의 장비와 600여 개에 이르는 공정 스텝
          </span>
          이 존재하며, 동일한 설비를 여러 번 반복하게 되는{" "}
          <span className="bold">사이클링 공정(Re-entrant)</span>을 가지는
          특징이 있습니다. 특히 주문형 반도체를 제조하는 공정에서는 수많은
          제품이 동시에 같은 설비들을 공유하여 더욱 큰 복잡성을 초래합니다.
          <br />
          <br />
          <span className="bold">적절한 웨이퍼 할당과 제품투입량 결정</span>이
          매우 중요한 의사결정 포인트이며, 생산라인은 웨이퍼(Wafer) 혹은
          로트(Lot)들이{" "}
          <span className="bold">유동적이고 연속적으로 흘러가도록 관리</span>가
          되어져야 합니다. 이를 통해 설비의 효율성을 높이고, 제조 사이클 타임을
          줄이는 것이 제조원가 경쟁력에 매우 중요합니다.
        </div>
      </section>

      <VerticalLine />

      <SolutionsTabArea />

      <HorizontalLine />

      <Reference companies={["hynix", "essencore"]} />

      <VerticalLine />

      <SimpleInquiry />

      <HorizontalBottomLine />
    </>
  )
}

export default SemiConductorContainer
