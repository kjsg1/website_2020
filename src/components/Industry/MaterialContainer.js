import React from "react"

import contimg03 from "../../images/contimg03.jpg"

import "./industry.css"

import VerticalLine from "../VerticalLine"
import HorizontalLine from "../HorizontalLine"
import HorizontalBottomLine from "../HorizontalBottomLine"
import SolutionsTabArea from "../SolutionsTabArea"
import SimpleInquiry from "../SimpleInquiry"
import Reference from "../Reference"

const MaterialContainer = () => {
  return (
    <>
      <VerticalLine />

      <section className="industry-section-1">
        <h3 className="industry-section-title">소재/부품 산업동향</h3>
        <div>
          <div className="industry-section-1-img">
            <img alt="배경이미지" src={contimg03} />
          </div>
          <div className="industry-section-1-content">
            최근 소재/부품에 대한 글로벌 관심이 높아지고 있습니다.
            <br />
            <br />
            모든 산업의 근간이 되는 소재/부품 산업은
            <br />
            21세기 자동차와 조선, 항공, 로봇, IT 등의 뿌리 산업이라 할 수
            있습니다.
            <br />
            <br />
            <span className="bold">기술자립도</span>가 근간인 기초산업이기에{" "}
            <span className="bold">
              <br />
              글로벌 산업 트랜드에 맞춰 기술경쟁력
            </span>
            을 갖춰야 합니다.
          </div>
          <div className="m_industry-section-1-content">
            최근 소재/부품에 대한 글로벌 관심이 높아지고 있습니다. 모든 산업의
            근간이 되는 소재/부품 산업은 21세기 자동차와 조선, 항공, 로봇, IT
            등의 뿌리 산업이라 할 수 있습니다.
            <span className="bold">
              <br />
              <br />
              기술자립도
            </span>
            가 근간인 기초산업이기에{" "}
            <span className="bold">글로벌 산업 트랜드에 맞춰 기술경쟁력</span>을
            갖춰야 합니다.
          </div>
        </div>
      </section>

      <HorizontalLine />

      <section className="industry-section-2">
        <h3 className="industry-section-title">공정 및 시스템 측면의 특징</h3>
        <div className="industry-section-2-content">
          전형적인 수요자 주도 시장으로{" "}
          <span className="bold">다품종 소량생산</span>이 많고,{" "}
          <span className="bold">단납기</span>의 특징을 가집니다.
          <br />
          <br />
          급변하는 소비자의 요구에 따라 부품을{" "}
          <span className="bold">적기 적소에 제공하는 것이 매우 중요</span>
          합니다.
          <br />
          이러한 특성으로 인해,
          <span className="bold">
            {" "}
            S&OP(Sales & Operation Planning) 프로세스
          </span>
          가 매우 중요하며,
          <br />
          S&OP와 생산 시스템의 구축과 연동이 중요한 산업입니다.
        </div>
        <div className="m_industry-section-2-content">
          전형적인 수요자 주도 시장으로{" "}
          <span className="bold">다품종 소량생산</span>이 많고,{" "}
          <span className="bold">단납기</span>의 특징을 가집니다. 급변하는
          소비자의 요구에 따라 부품을{" "}
          <span className="bold">적기 적소에 제공하는 것이 매우 중요</span>
          합니다.
          <br />
          <br />
          이러한 특성으로 인해,
          <span className="bold">
            {" "}
            S&OP(Sales & Operation Planning) 프로세스
          </span>
          가 매우 중요하며, S&OP와 생산 시스템의 구축과 연동이 중요한
          산업입니다.
        </div>
      </section>

      <VerticalLine />

      <SolutionsTabArea />

      <HorizontalLine />

      <Reference companies={["skietechnology", "nexflex"]} />

      <VerticalLine />

      <SimpleInquiry />

      <HorizontalBottomLine />
    </>
  )
}

export default MaterialContainer
