import React from "react"

import contimg05 from "../../images/contimg05.jpg"

import "./industry.css"

import VerticalLine from "../VerticalLine"
import HorizontalLine from "../HorizontalLine"
import HorizontalBottomLine from "../HorizontalBottomLine"
import SolutionsTabArea from "../SolutionsTabArea"
import SimpleInquiry from "../SimpleInquiry"
import Reference from "../Reference"

const ShipbuildingContainer = () => {
  return (
    <>
      <VerticalLine />

      <section className="industry-section-1">
        <h3 className="industry-section-title">조선 산업동향</h3>
        <div>
          <div className="industry-section-1-img">
            <img alt="배경이미지" src={contimg05} />
          </div>
          <div className="industry-section-1-content">
            조선 시장은 세계 단일 시장이라는 특성을 가집니다. 국적과 관계없이
            선주들은 자신이 원하는
            <br />
            선박을 가장 적정한 가격에 좋은 품질을 조선사에 주문을 합니다. 세계
            시장의 흐름은 국가별 특성
            <br />
            없이 국제해사기구 IMO 등의 정책에 따라 좌우되기도 하고, 주도적인
            기자재업체의 개발품에 따라
            <br />
            달라지기도 합니다.
            <br />
            따라서, 세계적 시장 요구를 읽고, 이에 대한 대안을 내놓는 것이
            중요합니다. 조선산업의 시황주기는 특히 길고, 필연적으로 오랜
            불황기가 발생하며, 이로 인해 한 나라의 조선산업을 무너뜨리기도 하고
            시장의 구조를 바꾸기도 합니다.
            <br />
            집중건조와 노후화 → 선복의 부족 → 호황 → 집중투자 → 공급과잉 →
            장기불황으로 이어지는 시황 주기는 피하기 어려우며, 필연적인
            것입니다. 위험을 낮추기 위해서 이러한 산업 특유의 특성을 파악
            <br />
            하고 장기적으로 위험 속에서 생존할 수 있는 전략을 강구해야 합니다.
            <br />
            해상환경 규제로 인한 <span className="bold">친환경·고효율화</span>와{" "}
            <span className="bold">선박의 스마트화</span> 및 생산공정의
            스마트화를
            <br />
            전략적 방향으로 위기를 넘어 지속가능성을 추구해야 합니다.
          </div>
          <div className="m_industry-section-1-content">
            조선 시장은 세계 단일 시장이라는 특성을 가집니다. 국적과 관계없이
            선주들은 자신이 원하는 선박을 가장 적정한 가격에 좋은 품질을
            조선사에 주문을 합니다. 세계 시장의 흐름은 국가별 특성 없이
            국제해사기구 IMO 등의 정책에 따라 좌우되기도 하고, 주도적인
            기자재업체의 개발품에 따라 달라지기도 합니다.
            <br />
            <br />
            따라서, 세계적 시장 요구를 읽고, 이에 대한 대안을 내놓는 것이
            중요합니다. 조선산업의 시황주기는 특히 길고, 필연적으로 오랜
            불황기가 발생하며, 이로 인해 한 나라의 조선산업을 무너뜨리기도 하고
            시장의 구조를 바꾸기도 합니다.
            <br />
            <br />
            집중건조와 노후화 → 선복의 부족 → 호황 → 집중투자 → 공급과잉 →
            장기불황으로 이어지는 시황 주기는 피하기 어려우며, 필연적인
            것입니다. 위험을 낮추기 위해서 이러한 산업 특유의 특성을 파악 하고
            장기적으로 위험 속에서 생존할 수 있는 전략을 강구해야 합니다.
            <br />
            <br />
            해상환경 규제로 인한 <span className="bold">친환경·고효율화</span>와{" "}
            <span className="bold">선박의 스마트화</span> 및 생산공정의
            스마트화를 전략적 방향으로 위기를 넘어 지속가능성을 추구해야 합니다.
          </div>
        </div>
      </section>

      <HorizontalLine />

      <section className="industry-section-2">
        <h3 className="industry-section-title">공정 및 시스템 측면의 특징</h3>
        <div className="industry-section-2-content">
          조선산업은{" "}
          <span className="bold">주문형 생산방식의 거대 조립산업</span>입니다.
          <br />
          <br />
          선박은 수송기계로 분류되지만 일반 제조업과는 달리 건축에 가까운 특성을
          가지고 있어
          <br />
          <span className="bold">표준화된 프로세스를 정립하기 어렵고</span>,
          조선소가 가지고 있는 공법기술과 생산인력의 역량에 따라
          <br />
          제품의 품질이 좌우되는 특성이 있습니다.
          <br />
          <br />
          이런 특징에 의해 조선산업에서의{" "}
          <span className="bold">MES는 진척관리, 공정 진행관리에 가까우며</span>
          <br />
          <span className="bold">
            시스템 구축과 운영을 위해 상당한 엔지니어링 지식이 필요
          </span>
          합니다.
        </div>
        <div className="m_industry-section-2-content">
          조선산업은{" "}
          <span className="bold">주문형 생산방식의 거대 조립산업</span>입니다.
          선박은 수송기계로 분류되지만 일반 제조업과는 달리 건축에 가까운 특성을
          가지고 있어{" "}
          <span className="bold">표준화된 프로세스를 정립하기 어렵고</span>,
          조선소가 가지고 있는 공법기술과 생산인력의 역량에 따라 제품의 품질이
          좌우되는 특성이 있습니다.
          <br />
          <br />
          이런 특징에 의해 조선산업에서의{" "}
          <span className="bold">
            MES는 진척관리, 공정 진행관리에 가까우며 시스템 구축과 운영을 위해
            상당한 엔지니어링 지식이 필요
          </span>
          합니다.
        </div>
      </section>

      <VerticalLine />

      <SolutionsTabArea />

      <HorizontalLine />

      <Reference companies={["hyundai"]} />

      <VerticalLine />

      <SimpleInquiry />

      <HorizontalBottomLine />
    </>
  )
}

export default ShipbuildingContainer
