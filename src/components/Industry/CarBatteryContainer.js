import React from "react"

import contimg02 from "../../images/contimg02.jpg"

import "./industry.css"

import VerticalLine from "../VerticalLine"
import HorizontalLine from "../HorizontalLine"
import HorizontalBottomLine from "../HorizontalBottomLine"
import SolutionsTabArea from "../SolutionsTabArea"
import SimpleInquiry from "../SimpleInquiry"
import Reference from "../Reference"

const CarBatteryContainer = () => {
  return (
    <>
      <VerticalLine />

      <section className="industry-section-1">
        <h3 className="industry-section-title">자동차/배터리 산업동향</h3>
        <div>
          <div className="industry-section-1-img">
            <img alt="배경이미지" src={contimg02} />
          </div>
          <div className="industry-section-1-content">
            자동차산업은 첨단기술이 결합한 미래차(전기차·자율주행차)를 중심으로
            <br />
            제조업에서 <span className="bold">'종합 모빌리티 산업'</span>으로
            진화하고 있습니다.
            <br />
            <br />
            미래차 산업 중심에는 '배터리 기술'의 발전이 있으며,
            <br />
            전기차 시장의 성장에 따라{" "}
            <span className="bold">차량용 2차 전지 시장</span> 또한 빠르게
            성장하고 있습니다.
            <br />
            <br />
            이와 같은 자동차산업의 패러다임 변화는{" "}
            <span className="bold">
              <br />
              인간의 근본적인 생활양식의 변화를 가속화
            </span>{" "}
            할 것입니다.
          </div>
          <div className="m_industry-section-1-content">
            자동차산업은 첨단기술이 결합한 미래차 (전기차·자율주행차)를 중심으로
            제조업에서 <span className="bold">'종합 모빌리티 산업'</span>으로
            진화하고 있습니다.
            <br />
            <br />
            미래차 산업 중심에는 '배터리 기술'의 발전이 있으며, 전기차 시장의
            성장에 따라 <span className="bold">차량용 2차 전지 시장</span> 또한
            빠르게 성장하고 있습니다.
            <br />
            <br />
            이와 같은 자동차산업의 패러다임 변화는{" "}
            <span className="bold">
              인간의 근본적인 생활양식의 변화를 가속화
            </span>{" "}
            할 것입니다.
          </div>
        </div>
      </section>

      <HorizontalLine />

      <section className="industry-section-2">
        <h3 className="industry-section-title">공정 및 시스템 측면의 특징</h3>
        <div className="industry-section-2-content">
          자동차 제조는 프레스, 차체, 도장 그리고 조립으로 구분할 수 있고,
          <br />
          앞의 <span className="bold">세 단계는 90% 이상 자동화가 구현</span>
          되어 있습니다.
          <br />
          이에 반해, 각종 부품을 장착하고 동작 상태를 점검하는 조립 공정은
          <br />
          작업의 복잡함으로 인하여{" "}
          <span className="bold">자동화의 비율이 가장 적은 부분</span>
          입니다.
          <br />
          이는 <span className="bold">MES의 중요성</span>이 그 어떤 부분보다
          효과적임을 반증합니다.
          <br />
          <br />이 과정에서 수백~수천의 1·2차 부품업체가 공급망에 포함되며,
          <br />
          다양한 방식의{" "}
          <span className="bold">
            공급방식(Push, Pull, Vendor Managed Inventory)
          </span>
          과<br />
          <span className="bold">생산방식(Build-To-Stock, Build-To-Order)</span>
          이<br />
          상황에 따라 적절히 조화되어야 합니다.
          <br />
          <span className="bold">SCM 운영</span> 전략과 방식을{" "}
          <span className="bold">시스템적으로 대응</span>할 수 있는 역량이 매우
          중요합니다. <br />
          <br />
          배터리 제조 공정의 가장 큰 특징은{" "}
          <span className="bold">완전자동화</span>입니다.
          <br />
          <span className="bold">수주·주문에 의해 제품의 설계와 생산</span>
          이 진행되며
          <br />
          생산 공정은 Cell - Module - Pack으로 이어집니다.
          <br />
          따라서{" "}
          <span className="bold">
            연구개발부터 생산-판매까지의 프로세스가 통합 관리
          </span>
          되는 것이 특징입니다.
          <br />
          잘못된 배터리 Cell 하나만으로도 전체 배터리팩의 성능에 부정적인 영향을
          미칠 수 있기 때문에,
          <br />
          Cell 생산 단계부터 <span className="bold">품질관리가 매우 중요</span>
          합니다.
        </div>
        <div className="m_industry-section-2-content">
          자동차 제조는 프레스, 차체, 도장 그리고 조립으로 구분할 수 있고, 앞의{" "}
          <span className="bold">세 단계는 90% 이상 자동화가 구현</span>
          되어 있습니다. 이에 반해, 각종 부품을 장착하고 동작 상태를 점검하는
          조립 공정은 작업의 복잡함으로 인하여{" "}
          <span className="bold">자동화의 비율이 가장 적은 부분</span>
          입니다. 이는 <span className="bold">MES의 중요성</span>이 그 어떤
          부분보다 효과적임을 반증합니다.
          <br />
          <br />이 과정에서 수백~수천의 1·2차 부품업체가 공급망에 포함되며,
          다양한 방식의{" "}
          <span className="bold">
            공급방식(Push, Pull, Vendor Managed Inventory)
          </span>
          과{" "}
          <span className="bold">생산방식(Build-To-Stock, Build-To-Order)</span>
          이 상황에 따라 적절히 조화되어야 합니다.
          <span className="bold"> SCM 운영</span> 전략과 방식을{" "}
          <span className="bold">시스템적으로 대응</span>할 수 있는 역량이 매우
          중요합니다. 배터리 제조 공정의 가장 큰 특징은{" "}
          <span className="bold">완전자동화</span>입니다.
          <br />
          <br />
          <span className="bold">수주·주문에 의해 제품의 설계와 생산</span>이
          진행되며 생산 공정은 Cell - Module - Pack으로 이어집니다. 따라서{" "}
          <span className="bold">
            연구개발부터 생산-판매까지의 프로세스가 통합 관리
          </span>
          되는 것이 특징입니다. 잘못된 배터리 Cell 하나만으로도 전체 배터리팩의
          성능에 부정적인 영향을 미칠 수 있기 때문에, Cell 생산 단계부터{" "}
          <span className="bold">품질관리가 매우 중요</span>
          합니다.
        </div>
      </section>

      <VerticalLine />

      <SolutionsTabArea />

      <HorizontalLine />

      <Reference companies={["skinnovation", "lgchem"]} />

      <VerticalLine />

      <SimpleInquiry />

      <HorizontalBottomLine />
    </>
  )
}

export default CarBatteryContainer
