import React from "react"

import contimg04 from "../../images/contimg04.jpg"

import "./industry.css"

import VerticalLine from "../VerticalLine"
import HorizontalLine from "../HorizontalLine"
import HorizontalBottomLine from "../HorizontalBottomLine"
import SolutionsTabArea from "../SolutionsTabArea"
import SimpleInquiry from "../SimpleInquiry"
import Reference from "../Reference"

const PetroChemContainer = () => {
  return (
    <>
      <VerticalLine />

      <section className="industry-section-1">
        <h3 className="industry-section-title">에너지·화학 산업동향</h3>
        <div>
          <div className="industry-section-1-img">
            <img alt="배경이미지" src={contimg04} />
          </div>
          <div className="industry-section-1-content">
            석유와 가스, 석탄 등을 원료로 석유제품과 화학제품을 생산하는
            산업으로,
            <br />
            크게 <span className="bold">정유로 대표되는 석유산업</span>과
            석유(나프타) 또는 가스를 원료로 자동차, 전자, 섬유 등<br />
            각종 산업에 필요한{" "}
            <span className="bold">
              화학소재(B2B 중간재)를 공급하는 석유화학산업
            </span>
            으로 구분됩니다.
            <br />
            <br />
            우리나라 제조업 분야의 주력산업 중 하나로, 세계적으로도 높은 위상을
            차지하고 있습니다. <br />
            다만 급격한 유가변동과 글로벌 공급/수요에 크게 영향을 받는
            산업이며, 전기차 비중 확대로
            <br />
            인한 <span className="bold">석유 수요 감소</span>, 중국과 중동,
            미국의 <span className="bold">석유화학 공급 증가</span>로 인해
            업황은 둔화하고 있는
            <br />
            상황입니다. <br />
            <br />
            중장기적으로 석유사업에 대한 장기전망이 불투명해지고 있고, 석유화학
            산업은 안정적
            <br />
            이면서 양호한 수익성을 기록하는 등
            <span className="bold">석유시장의 패러다임이 변화</span>하고 있기
            때문에{" "}
            <span className="bold">
              정유-석유
              <br />
              화학의 통합과 COTC(Crude Oil to Chemical) 설비에 대한 투자
            </span>
            가 필요한 시점입니다.
          </div>
          <div className="m_industry-section-1-content">
            석유와 가스, 석탄 등을 원료로 석유제품과 화학제품을 생산하는
            산업으로, 크게{" "}
            <span className="bold">정유로 대표되는 석유산업</span>과
            석유(나프타) 또는 가스를 원료로 자동차, 전자, 섬유 등 각종 산업에
            필요한{" "}
            <span className="bold">
              화학소재(B2B 중간재)를 공급하는 석유화학산업
            </span>
            으로 구분됩니다.
            <br />
            <br />
            우리나라 제조업 분야의 주력산업 중 하나로, 세계적으로도 높은 위상을
            차지하고 있습니다.
            <br />
            다만 급격한 유가변동과 글로벌 공급/수요에 크게 영향을 받는
            산업이며, 전기차 비중 확대로 인한{" "}
            <span className="bold">석유 수요 감소</span>, 중국과 중동, 미국의{" "}
            <span className="bold">석유화학 공급 증가</span>로 인해 업황은
            둔화하고 있는 상황입니다.
            <br />
            <br />
            중장기적으로 석유사업에 대한 장기전망이 불투명해지고 있고, 석유화학
            산업은 안정적 이면서 양호한 수익성을 기록하는 등
            <span className="bold">석유시장의 패러다임이 변화</span>하고 있기
            때문에{" "}
            <span className="bold">
              정유-석유 화학의 통합과 COTC(Crude Oil to Chemical) 설비에 대한
              투자
            </span>
            가 필요한 시점입니다.
          </div>
        </div>
      </section>

      <HorizontalLine />

      <section className="industry-section-2">
        <h3 className="industry-section-title">공정 및 시스템 측면의 특징</h3>
        <div className="industry-section-2-content">
          증류탑(Distillation Column)과 열수지와 물질수지(Heat & Mass balance)
          계산 등 화학공학 지식을 베이스로 하며,
          <br />
          단계별로 복잡한 시스템과 단위공정들이 있습니다.
          <br />
          <br />
          단위 공정 설비자동화 수준이 높고, 각 공정이 연속적으로 연결되어있기
          때문에
          <br />
          IT 기술을 통한{" "}
          <span className="bold">단위공정 간 연계성 확보와 가시성 확보</span>
          가 중요합니다.
          <br />
          <br />
          또한 IT시스템을 통해 수요예측, 구매관리, 통합생산관리 등 전문영역의
          효율적 운영뿐 아니라{" "}
          <span className="bold">
            <br />
            전체 기업활동의 최적화
          </span>
          를 달성하는 것이 중요한 과제입니다.
        </div>
        <div className="m_industry-section-2-content">
          증류탑(Distillation Column)과 열수지와 물질수지(Heat & Mass balance)
          계산 등 화학공학 지식을 베이스로 하며, 단계별로 복잡한 시스템과
          단위공정들이 있습니다.
          <br />
          <br />
          단위 공정 설비자동화 수준이 높고, 각 공정이 연속적으로 연결되어있기
          때문에 IT 기술을 통한{" "}
          <span className="bold">단위공정 간 연계성 확보와 가시성 확보</span>
          가 중요합니다.
          <br />
          <br />
          또한 IT시스템을 통해 수요예측, 구매관리, 통합생산관리 등 전문영역의
          효율적 운영뿐 아니라{" "}
          <span className="bold">전체 기업활동의 최적화</span>를 달성하는 것이
          중요한 과제입니다.
        </div>
      </section>

      <VerticalLine />

      <SolutionsTabArea />

      <HorizontalLine />

      <Reference companies={["skinnovation", "lgchem", "logolotteche"]} />

      <VerticalLine />

      <SimpleInquiry />

      <HorizontalBottomLine />
    </>
  )
}

export default PetroChemContainer
