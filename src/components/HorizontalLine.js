import React from "react"
import w_square from "../images/w_square.jpg"

import "./horizontalLine.css"

const HorizontalLine = () => (
  <div className="horizontal-line">
    <img alt="가로선" src={w_square} />
  </div>
)

export default HorizontalLine
