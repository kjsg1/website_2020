import React, { useState } from "react"
import { Link } from "gatsby"
import tabimg01 from "../images/solutiontabimg01.jpg"
import tabimg02 from "../images/solutiontabimg02.jpg"
import tabimg03 from "../images/solutiontabimg03.jpg"
import tabimg05 from "../images/solutiontabimg05.jpg"
import tabimg06 from "../images/solutiontabimg06.jpg"

import "./solutionsTabArea.css"

const labelStyle = selected => {
  if (selected) {
    return {
      borderBottom: "4px solid #ff0000",
    }
  }
  return {}
}

const TabContent1 = () => (
  <div className="solutions-tab-content-container">
    <div className="solutions-tab-content-text">
      <div className="solutions-tab-content-title">NFS MES</div>
      <div>
        <p>인사이트온의 MES 솔루션으로 제조 효율성과 생산성을 극대화하세요.</p>
        <br />
        <p>· 현장의 생산진행 현황의 실시간 모니터링 및 통제</p>
        <p>· Smart 대차와 Kanban 제공</p>
      </div>
    </div>
    <div className="solutions-tab-content-img">
      <Link to="/solutions/operation">
        <img width="100%" alt="아이콘" src={tabimg01} />
      </Link>
    </div>
  </div>
)

const TabContent2 = () => (
  <div className="solutions-tab-content-container">
    <div className="solutions-tab-content-text">
      <div className="solutions-tab-content-title">NFS WMS</div>
      <div>
        <p>· IoT 기술, 스마트 기기를 통해 Smart 재고 실사</p>
        <p></p>
        <p>
          · 입고, 적차, 재고, 피킹, 출고 등 물류센터 프로세스 전체 통합 관리
        </p>
      </div>
    </div>
    <div className="solutions-tab-content-img">
      <Link to="/solutions/logistics">
        <img width="100%" alt="아이콘" src={tabimg02} />
      </Link>
    </div>
  </div>
)

const TabContent3 = () => (
  <div className="solutions-tab-content-container">
    <div className="solutions-tab-content-text">
      <div className="solutions-tab-content-title">Lab OneQ</div>
      <div>
        <p>· 클라우드 기반의 구독형 LIMS(실험정보관리시스템)</p>
        <p>· 화학제조, 제약, 식품제조 산업의 품질관리를 위한 솔루션</p>
      </div>
    </div>
    <div className="solutions-tab-content-img">
      <Link to="/solutions/quality">
        <img width="100%" alt="아이콘" src={tabimg03} />
      </Link>
    </div>
  </div>
)

const TabContent5 = () => (
  <div className="solutions-tab-content-container">
    <div className="solutions-tab-content-text">
      <div className="solutions-tab-content-title">NFS Pbot (PLC bot)</div>
      <div>
        <p>
          Smart Factory 구축에 최대의 장애요소인 자동화 설비와
          <br />
          MES 연동 문제를 해결하기 위한 솔루션입니다.
        </p>
        <br />
        <p>· Workflow 다이어그램 작성을 통한 설비통신 로직 완성</p>
        <p>· 설비 셋업 전 사전 시뮬레이션과 장애 및 사고 상황 테스트</p>
        <p>· 다양한 설비 프로토콜과 산업표준(OPC, PLC 등) 지원</p>
      </div>
    </div>
    <div className="solutions-tab-content-img">
      <Link to="/solutions/automation">
        <img width="100%" alt="아이콘" src={tabimg05} />
      </Link>
    </div>
  </div>
)

const TabContent6 = () => (
  <div className="solutions-tab-content-container">
    <div className="solutions-tab-content-text">
      <div className="solutions-tab-content-title">스마트공장 개발방법론</div>
      <div>
        <p>인사이트온만의 스마트공장 개발방법론입니다.</p>
        <br />
        <p>공정에 대한 높은 이해와 관리대상 생애주기의 모델링을 기반으로</p>
        <p>스마트공장 시스템을 조기에 출시하고, 점진적으로 개선합니다.</p>
      </div>
    </div>
    <div className="solutions-tab-content-img">
      <Link to="/solutions/methodology">
        <img width="100%" alt="아이콘" src={tabimg06} />
      </Link>
    </div>
  </div>
)

const SolutionsTabArea = () => {
  const [selectedTab, setSelectedTab] = useState(1)

  return (
    <>
      <section className="solutions-section">
        <h3 className="solutions-title">솔루션</h3>
        <div className="solutions-tab-box-bg">
          <div className="solutions-tab-box-no-bg">
            <div className="solutions-tab-box-inside">
              <div className="solutions-tab-labels-wrapper">
                <div
                  className="solutions-tab-label"
                  style={labelStyle(selectedTab === 1)}
                  onClick={() => setSelectedTab(1)}
                >
                  <div>생산관리</div>
                </div>
                <div
                  className="solutions-tab-label"
                  style={labelStyle(selectedTab === 2)}
                  onClick={() => setSelectedTab(2)}
                >
                  <div>물류관리</div>
                </div>
                <div
                  className="solutions-tab-label"
                  style={labelStyle(selectedTab === 3)}
                  onClick={() => setSelectedTab(3)}
                >
                  <div>품질관리</div>
                </div>
                <div
                  className="solutions-tab-label"
                  style={labelStyle(selectedTab === 5)}
                  onClick={() => setSelectedTab(5)}
                >
                  <div>설비자동화</div>
                </div>
                <div
                  className="solutions-tab-label"
                  style={labelStyle(selectedTab === 6)}
                  onClick={() => setSelectedTab(6)}
                >
                  <div>
                    스마트공장
                    <br />
                    개발방법론
                  </div>
                </div>
              </div>

              {selectedTab === 1 ? <TabContent1 /> : null}
              {selectedTab === 2 ? <TabContent2 /> : null}
              {selectedTab === 3 ? <TabContent3 /> : null}
              {selectedTab === 5 ? <TabContent5 /> : null}
              {selectedTab === 6 ? <TabContent6 /> : null}
            </div>
          </div>
        </div>
      </section>
      <section className="m-solutions-section">
        <h3 className="solutions-title">솔루션</h3>
        <div className="m_solutionbgbox">
          <div className="m_solutions-content">
            <div className="m_solution-title">생산관리</div>
            <div>
              <div className="solutions-tab-content-title">NFS MES</div>
              <div className="solutions-tab-content-img">
                <Link to="/solutions/operation">
                  <img width="70%" alt="아이콘" src={tabimg01} />
                </Link>
              </div>
              <div>
                <p className="solutions-tab-content-text">
                  · 생산 현황 실시간 모니터링 및 통제
                  <br />· Smart 대차와 Kanban 제공
                </p>
              </div>
            </div>
          </div>
          <div className="m_solutions-content">
            <div className="m_solution-title">물류관리</div>
            <div>
              <div className="solutions-tab-content-title">NFS WMS</div>
              <div className="solutions-tab-content-img">
                <Link to="/solutions/logistics">
                  <img width="70%" alt="아이콘" src={tabimg02} />
                </Link>
              </div>
              <div>
                <p className="solutions-tab-content-text1">
                  · Smart 재고 실사
                  <br />· 물류 센터 프로세스 전체 통합 관리
                </p>
              </div>
            </div>
          </div>
          <div className="m_solutions-content">
            <div className="m_solution-title">품질관리</div>
            <div>
              <div className="solutions-tab-content-title">Lab OneQ</div>
              <div className="solutions-tab-content-img">
                <Link to="/solutions/quality">
                  <img width="70%" alt="아이콘" src={tabimg03} />
                </Link>
              </div>
              <div>
                <p className="solutions-tab-content-text1">
                  · 클라우드 기반의 구독형 LIMS
                  <br />· 화학제조, 제약, 식품제조 산업의 품질관리를 위한 솔루션
                </p>
              </div>
            </div>
          </div>
          <div className="m_solutions-content">
            <div className="m_solution-title">설비자동화</div>
            <div>
              <div className="solutions-tab-content-title">
                NFS Pbot (PLC bot)
              </div>
              <div className="solutions-tab-content-img">
                <Link to="/solutions/automation">
                  <img width="70%" alt="아이콘" src={tabimg05} />
                </Link>
              </div>
              <div>
                <p className="solutions-tab-content-text">
                  자동화 설비와 MES 연동문제 해결을 위한 솔루션
                </p>
              </div>
            </div>
          </div>
          <div className="m_solutions-content">
            <div className="m_solution-title">스마트공장 개발방법론</div>
            <div>
              <div className="solutions-tab-content-title">AOM Methodology</div>
              <div className="solutions-tab-content-img">
                <Link to="/solutions/methodology">
                  <img width="70%" alt="아이콘" src={tabimg06} />
                </Link>
              </div>
              <div>
                <p className="solutions-tab-content-text2">
                  인사이트온만의 스마트공장 개발방법론.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

export default SolutionsTabArea
