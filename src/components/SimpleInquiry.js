import React from "react"

import "./simpleInquiry.css"

const SimpleInquiry = () => (
  <section className="simple-inquiry-section">
    <div className="title_h3">문의하기</div>
    <div className="simple-inquiry-text">
      궁금하신 내용이 있으신가요? 문의하여 주시면 빠른 답변드리겠습니다.
    </div>
    <div className="m_simple-inquiry-text">
      궁금하신 내용이 있으신가요?
      <br />
      문의하여 주시면 빠른 답변드리겠습니다.
    </div>
    <div className="simple-inquiry-button-container">
      <a href="https://forms.gle/cC5JYqQfFSwDx9VJ9" target="_blank" rel="noreferrer">
        <div className="simple-inquiry-button">문의하기</div>
      </a>
    </div>
  </section>
)

export default SimpleInquiry
