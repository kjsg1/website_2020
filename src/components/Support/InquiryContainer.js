import React from "react"

import VerticalLine from "../VerticalLine"
import HorizontalBottomLine from "../HorizontalBottomLine"

const InquiryContainer = () => {

  return (
    <>
      <VerticalLine />

      <section style={{ display: "flex", justifyContent: "center" }}>
          <iframe
            src="https://docs.google.com/forms/d/e/1FAIpQLScDXETUSK3QA-doogweDGQvzIj3BWtJCN0ITrMZDA188fJ8rw/viewform?embedded=true"
            width="800"
            height="1330"
            frameBorder="0"
            marginHeight="0"
            marginWidth="0"
            title="inquiry"
          >
            로드 중…
          </iframe>
        </section>

      <HorizontalBottomLine />
    </>
  )
}
export default InquiryContainer
