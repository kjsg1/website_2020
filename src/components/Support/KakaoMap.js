import React from "react"

const divStyle = {
  font: "normal normal 400 12px/normal dotum, sans-serif",
  width: "100%",
  height: "432px",
  color: "#333",
  position: "relative",
}

const KakaoMap = () => (
  <div className="container">
    <div style={divStyle}>
      <div style={{ height: "400px", borderTop: "5px solid #000" }}>
        <a
          href="https://map.kakao.com/?urlX=1016845.0&amp;urlY=579964.0&amp;itemId=602120705&amp;q=%EC%9D%B8%EC%82%AC%EC%9D%B4%ED%8A%B8%EC%98%A8&amp;srcid=602120705&amp;map_type=TYPE_MAP&amp;from=roughmap"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img
            alt="인사이트온 지도"
            className="kakao-map-img"
            src="//t1.daumcdn.net/roughmap/imgmap/c29752700527220fd6277eb8bd67072dad999aed49e4952ed77308f65ff41b02"
            width="100%"
            height="398px"
            style={{ borderBottom: "1px solid #ccc" }}
          />
        </a>
      </div>
      <div
        style={{
          overflow: "hidden",
          padding: "7px 11px",
          border: "1px solid rgba(0, 0, 0, 0.1)",
          borderRadius: "0px 0px 2px 2px",
          backgroundColor: "rgb(249, 249, 249)",
        }}
      >
        <a
          href="https://map.kakao.com"
          target="_blank"
          style={{ float: "left" }}
          rel="noopener noreferrer"
        >
          <img
            src="//t1.daumcdn.net/localimg/localimages/07/2018/pc/common/logo_kakaomap.png"
            width="72"
            height="16"
            alt="카카오맵"
            style={{ display: "block", width: "72px", height: "16px" }}
          />
        </a>
        <div
          style={{
            float: "right",
            position: "relative",
            top: "1px",
            fontSize: "11px",
          }}
        >
          <a
            target="_blank"
            href="https://map.kakao.com/?from=roughmap&amp;srcid=602120705&amp;confirmid=602120705&amp;q=%EC%9D%B8%EC%82%AC%EC%9D%B4%ED%8A%B8%EC%98%A8&amp;rv=on"
            style={{
              float: "left",
              height: "15px",
              paddingTop: "1px",
              lineHeight: "15px",
              color: "#000",
              textDecoration: "none",
            }}
            rel="noopener noreferrer"
          >
            로드뷰
          </a>
          <span
            style={{
              width: "1px",
              padding: 0,
              margin: "0 8px 0 9px",
              height: "11px",
              verticalAlign: "top",
              position: "relative",
              top: "2px",
              borderLeft: "1px solid #d0d0d0",
              float: "left",
            }}
          ></span>
          <a
            target="_blank"
            href="https://map.kakao.com/?from=roughmap&amp;eName=%EC%9D%B8%EC%82%AC%EC%9D%B4%ED%8A%B8%EC%98%A8&amp;eX=1016845.0&amp;eY=579964.0"
            style={{
              float: "left",
              height: "15px",
              paddingTop: "1px",
              lineHeight: "15px",
              color: "#000",
              textDecoration: "none",
            }}
            rel="noopener noreferrer"
          >
            길찾기
          </a>
          <span
            style={{
              width: "1px",
              padding: 0,
              margin: "0 8px 0 9px",
              height: "11px",
              verticalAlign: "top",
              position: "relative",
              top: "2px",
              borderLeft: "1px solid #d0d0d0",
              float: "left",
            }}
          ></span>
          <a
            target="_blank"
            href="https://map.kakao.com?map_type=TYPE_MAP&amp;from=roughmap&amp;srcid=602120705&amp;itemId=602120705&amp;q=%EC%9D%B8%EC%82%AC%EC%9D%B4%ED%8A%B8%EC%98%A8&amp;urlX=1016845.0&amp;urlY=579964.0"
            style={{
              float: "left",
              height: "15px",
              paddingTop: "1px",
              lineHeight: "15px",
              color: "#000",
              textDecoration: "none",
            }}
            rel="noopener noreferrer"
          >
            지도 크게 보기
          </a>
        </div>
      </div>
    </div>
  </div>
)

export default KakaoMap
