import React from "react"

import VerticalLine from "../VerticalLine"
import HorizontalBottomLine from "../HorizontalBottomLine"
import KakaoMap from "./KakaoMap"

import "./contactContainer.css"

const ContactContainer = () => (
  <>
    <VerticalLine />

    <div className="mapinsighton">
      <h3 className="title_h3">찾아오시는 길</h3>
      <div>
        <KakaoMap />
      </div>
      <div className="mapinsightontext">
        <span className="bold">(본사)</span> 울산 중구 종가로 15, 울산테크노파크
        기술혁신B동 402호
        <br />
        <span className="bold">(서울)</span> 서울 강남구 강남대로 132길4, 2층 (논현동, 진빌딩)
        <br />
        <span className="bold">(대표번호)</span> 052-248-5188
      </div>
    </div>

    <HorizontalBottomLine />
  </>
)

export default ContactContainer
