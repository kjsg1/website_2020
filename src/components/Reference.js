import React from "react"
import PropTypes from "prop-types"

import logoSkcc from "../images/logo_skcc.png"
import logoSkinno from "../images/logo_skinno.png"
import logoSktech from "../images/logo_sktech.png"
import logoEss from "../images/logo_ess.png"
import logoLgcns from "../images/logo_lgcns.png"
import logoLgch from "../images/logo_lgch.png"
import logoCj from "../images/logo_cj.png"
import logoChangs from "../images/logo_changs.png"
import logoNexflex from "../images/logo_nexflex.png"
import logoHyyundai from "../images/logo_hyd.png"
import logoHynix from "../images/logo_skhynix.png"
import logolotteche from "../images/logo_lotteche.png"

import "./reference.css"

const ReferenceSectionContainer = ({ companies }) => {
  const logosJsx = []

  companies.forEach((company, i) => {
    let logo;//, name
    switch (company) {
      case "skcc":
        logo = logoSkcc
        //name = "SK주식회사 C&C"
        break
      case "skinnovation":
        logo = logoSkinno
        //name = "SK이노베이션"
        break
      case "skietechnology":
        logo = logoSktech
        //name = "SK아이이테크놀로지"
        break
      case "essencore":
        logo = logoEss
        //name = "에센코어"
        break
      case "lgcns":
        logo = logoLgcns
        //name = "LG씨엔에스"
        break
      case "lgchem":
        logo = logoLgch
        //name = "LG화학"
        break
      case "cj":
        logo = logoCj
        //name = "CJ제일제당"
        break
      case "changshin":
        logo = logoChangs
        //name = "창신Inc"
        break
      case "nexflex":
        logo = logoNexflex
        //name = "넥스플렉스"
        break
      case "hynix":
        logo = logoHynix
        //name = "SK하이닉스"
        break
      case "hyundai":
        logo = logoHyyundai
        //name = "현대중공업"
        break
      case "logolotteche":
        logo = logolotteche
        //name = "롯데정밀화학"
        break
      default:
        return
    }

    logosJsx.push(
      <div key={`logo_${i}`} className="reference-img-box">
        <img className="reference-logo" alt="로고" src={logo} />
      </div>
    )
  })

  return (
    <section className="reference-section">
      <h3 className="reference-title">Reference</h3>
      <div className="container">
        <div className="reference-row">{logosJsx}</div>
      </div>
    </section>
  )
}

ReferenceSectionContainer.defaultProps = {
  companies: [],
}

ReferenceSectionContainer.propTypes = {
  companies: PropTypes.arrayOf(PropTypes.string),
}

export default ReferenceSectionContainer
