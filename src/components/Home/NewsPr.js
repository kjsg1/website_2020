import React from "react"

import "./newsPr.css"
import NewsBox from "../Company/NewsBox"

import news_20210329_1 from "../../images/news_20210329-1.jpg"
import news_201125 from "../../images/news_201125.jpg"
import news_20220722 from "../../images/news_20220722.jpg"

const NewsPr = () => (
  <>
    <section className="main-news-section">
      <div className="main-news-title">
        News <span className="light">&amp;</span> PR
      </div>
      <div className="main-news-boxes-container">
        <NewsBox
          img={news_20220722}
          type="기사"
          title="2022년 울산 스타기업 지정"
          date="2022.07.22"
          link="m.etnews.com/20220722000053?obj=Tzo4OiJzdGRDbGFzcyI6Mjp7czo3OiJyZWZlcmVyIjtOO3M6NzoiZm9yd2FyZCI7czoxMzoid2ViIHRvIG1vYmlsZSI7fQ%3D%3D"
        />
        <NewsBox
          img={news_20210329_1}
          type="기사"
          title="울산 중구, 4차산업기반 IT기업 대표 간담회 초청"
          date="2021.03.26"
          link="ulsanpress.net/news/articleView.html?idxno=373972"
        />
        <NewsBox
          img={news_201125}
          type="홍보"
          title="2020 울산 스타트업 TOP10 선정"
          date="2020.11.25"
          link="blog.naver.com/insight_on/222154227032"
        />
      </div>
    </section>
  </>
)

export default NewsPr
