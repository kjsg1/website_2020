import React from "react"

import "./bestSolution.css"

const BestSolution = () => (
  <>
    <section className="best-solution-section">
      <div className="container">
        <h4 className="smallt">INSIGHT ON'S BEST Solution</h4>
        <h1 className="title_l1">
          스마트공장 모델링
          <span className="light">(API Modeling Tool)</span>
          <br />
          자동화 제품
        </h1>
        <h1 className="m_title_l1">
          스마트공장 모델링
          <span className="light">(API Modeling Tool)</span> 자동화 제품
        </h1>
        <p className="align2w">
          Smart Factory를 구성하는 수많은 시스템을 유기적으로
          <br />
          연동하기 위한 API 모델링 솔루션입니다.
          <br />
          인사이트온의 솔루션뿐만 아니라 Legacy 시스템과의 연결도
          <br />
          지원하여 더 쉽고 빠른 스마트공장 시스템 구축과 자동화를 실현합니다.
        </p>
        <p className="m_align2w">
          Smart Factory를 구성하는 수많은 시스템을 유기적으로 연동하기 위한
          API 모델링 솔루션입니다. 인사이트온의 솔루션뿐만 아니라 Legacy
          시스템과의 연결도 지원하여 더 쉽고 빠른 스마트공장 시스템 구축과
          자동화를 실현합 니다.
        </p>
      </div>
    </section>
  </>
)

export default BestSolution
