import React from "react"
import { Link } from "gatsby"

import "./support.css"

const Support = () => (
  <>
    <section className="home-support-section" id="content6">
      <div className="home-support-container">
        <h1 className="home-support-title">고객지원</h1>
        <p className="home-support-text">
          궁금하신 내용이 있으신가요? 문의하여 주시면 빠른 답변드리겠습니다.
        </p>
        <p className="m_home-support-text">
          궁금하신 내용이 있으신가요?
          <br />
          문의하여 주시면 빠른 답변드리겠습니다.
        </p>

        <a href="https://forms.gle/cC5JYqQfFSwDx9VJ9" target="_blank">
          <div className="home-support-btn">
            <div className="home-support-btn-text">문의하기</div>
          </div>
        </a>
      </div>
    </section>
  </>
)

export default Support
