import React from "react"
import { Link } from "gatsby"

import "./development.css"

import aswell01 from "../../images/aswell01.jpg"
import aswell02 from "../../images/aswell02.jpg"
import aswell03 from "../../images/aswell03.jpg"

const Development = () => (
  <section className="home-development-section" id="content3">
    <h1 className="title_l">
      <span className="light">As well,</span> INSIGHT ON
      <span className="light">!</span>
    </h1>
    <p className="align2pas">
      인사이트온은 글로벌 기업으로 도약을 위해 솔루션개발뿐만 아니라, 요소 기술
      개발에도 매진하고 있습니다.
      <br />
      또한, 다양한 컨설팅 경험을 통해 최상의 서비스를 제공할 수 있는 역량을
      보유하고 있습니다.
    </p>
    <p className="m_align2pas">
      인사이트온은 글로벌 기업으로 도약을 위해 솔루션개발뿐만 아니라, 요소
      기술 개발에도 매진하고 있습니다. 또한, 다양한 컨설팅 경험을 통해 최상의
      서비스를 제공할 수 있는 역량을 보유하고 있습니다.
    </p>
    <Link to="/company/certificates">
      <div className="home-development-content3box">
        <div className="home-development-content-box">
          <div>
            <img
              alt="이미지"
              className="home-development-content-image"
              src={aswell01}
            />
          </div>
          <h3 className="home-development-content-title">
            2019 대한민국 우수특허 대상
          </h3>
          <p className="home-development-content-description">
            기업과 프리랜서 매칭 방법 및 <br />
            시스템에 대한 특허와 플랫폼개발
          </p>
        </div>
        <div className="home-development-content-box">
          <div>
            <img
              className="home-development-content-image"
              alt="이미지"
              src={aswell02}
            />
          </div>
          <h3 className="home-development-content-title">
            제조실행시스템 설계를 위한
            <br />
            방법 및 시스템
          </h3>
          <p className="home-development-content-description">
            MES 설계의 용이성을 위한
            <br />
            요소기술 연구개발
          </p>
        </div>
        <div className="home-development-content-box">
          <div>
            <img
              className="home-development-content-image"
              alt="이미지"
              src={aswell03}
            />
          </div>
          <h3 className="home-development-content-title">
            설비통신 자동화 방법 및 시스템
          </h3>
          <p className="home-development-content-description">
            Rule 기반의 PLC와 MES 간 통신
            <br />
            자동화를 위한 요소기술 연구개발
          </p>
        </div>
      </div>
    </Link>
  </section>
)

export default Development
