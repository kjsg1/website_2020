import React from "react"
import { Link } from "gatsby"

import "./reference.css"

import logoSkcc from "../../images/logo_skcc.png"
import logoSkinno from "../../images/logo_skinno.png"
import logoSktech from "../../images/logo_sktech.png"
import logoEss from "../../images/logo_ess.png"
import logoLgcns from "../../images/logo_lgcns.png"
import logoLgch from "../../images/logo_lgch.png"
import logoCj from "../../images/logo_cj.png"
import logoChangs from "../../images/logo_changs.png"
import logoskhynix from "../../images/logo_skhynix.png"
import logolotteche from "../../images/logo_lotteche.png"
import logohyd from "../../images/logo_hyd.png"
import logoNexFlex from "../../images/logo_nexflex.png"
import logoSkc from "../../images/logo_skc.png"
import logoAveva from "../../images/logo_aveva.png"
import logoKumho from "../../images/logo_kumho.png"

const Reference = ({ isInMainPage }) => {
  const contents = (
    <div className="container">
      <div className="main-reference-row">
        <div className="main-reference-img-box">
          <img className="main-reference-logo" alt="SKC" src={logoSkc} />
        </div>

        <div className="main-reference-img-box">
          <img
            alt="SK주식회사 C&amp;C"
            className="main-reference-logo"
            src={logoSkcc}
          />
        </div>

        <div className="main-reference-img-box">
          <img
            className="main-reference-logo"
            alt="SK하이닉스"
            src={logoskhynix}
          />
        </div>

        <div className="main-reference-img-box">
          <img
            className="main-reference-logo"
            alt="SK이노베이션"
            src={logoSkinno}
          />
        </div>
      </div>
      <div className="main-reference-row">
        <div className="main-reference-img-box">
          <img
            className="main-reference-logo"
            alt="SK아이이테크놀로지"
            src={logoSktech}
          />
        </div>

        <div className="main-reference-img-box">
          <img
            className="main-reference-logo"
            alt="LG씨엔에스"
            src={logoLgcns}
          />
        </div>
        <div className="main-reference-img-box">
          <img className="main-reference-logo" alt="LG화학" src={logoLgch} />
        </div>

        <div className="main-reference-img-box">
          <img className="main-reference-logo" alt="제일제당" src={logoCj} />
        </div>
      </div>
      <div className="main-reference-row">
        <div className="main-reference-img-box">
          <img
            className="main-reference-logo"
            alt="창신 주식회사"
            src={logoChangs}
          />
        </div>

        <div className="main-reference-img-box">
          <img
            className="main-reference-logo"
            alt="롯데정밀화학"
            src={logolotteche}
          />
        </div>

        <div className="main-reference-img-box">
          <img className="main-reference-logo" alt="에센코어" src={logoEss} />
        </div>
        <div className="main-reference-img-box">
          <img
            className="main-reference-logo"
            alt="넥스플렉스"
            src={logoNexFlex}
          />
        </div>
      </div>
      <div className="main-reference-row">
        <div className="main-reference-img-box">
          <img className="main-reference-logo" alt="aveva" src={logoAveva} />
        </div>
        <div className="main-reference-img-box">
          <img className="main-reference-logo" alt="현대중공업" src={logohyd} />
        </div>
        <div className="main-reference-img-box">
          <img
            className="main-reference-logo"
            alt="금호석유화학"
            src={logoKumho}
          />
        </div>
      </div>
    </div>
  )

  return (
    <>
      <section
        className={`${
          isInMainPage ? "main-reference-section" : "accomplish_box"
        }`}
      >
        {isInMainPage ? (
          <Link to="/accomplishments">
            <h1 className="title_l">Reference</h1>
            {contents}
          </Link>
        ) : (
          <>
            <h3 className="accomplish_title">주요 고객사</h3>
            {contents}
          </>
        )}
      </section>
    </>
  )
}
export default Reference
