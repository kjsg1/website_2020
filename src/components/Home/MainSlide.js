import React from "react"
import Slider from "react-slick"

import "./mainSlide.css"
import "slick-carousel/slick/slick.css"

import backgroundImg1 from "../../images/main_visual01.jpg"
import backgroundImg2 from "../../images/main_visual02.jpg"
import backgroundImg3 from "../../images/main_visual03.jpg"
import objectImg3 from "../../images/main_visual03_object.png"
import logosmall from "../../images/logosmall.png"

const slide1BackgroundStyle = img => ({
  background: `url("${img}") no-repeat center center`,
  height: "695px",
  backgroundSize: "cover",
  textAlign: "center",
})

const MainSlide = () => {
  const settings = {
    fade: true,
    infinite: true,
    autoplaySpeed: 7000,
    autoplay: true,
    pauseOnHover: false,
    dots: false,
    arrows: false,
  }

  return (
    <div className="main-slide-container">
      <Slider {...settings}>
        <div>
          <div style={slide1BackgroundStyle(backgroundImg1)}>
            <div className="container">
              <div className="main-slide-text-wrapper-1">
                <div className="m_main-slide-text-title1">
                  Enabling Smart Factory in a day. Enhance it everyday.
                </div>
                <div className="main-slide-text-title">
                  Enabling Smart Factory in a day.
                  <br />
                  Enhance it everyday.
                </div>
                <div className="main-slide-text-small">
                  단 하루 만에도 스마트팩토리는 가능해질 수 있습니다.
                  <br />
                  그리고 매일 매일 더 강화될 것입니다.
                </div>
              </div>
            </div>
          </div>
        </div>
        <div>
          <div style={slide1BackgroundStyle(backgroundImg2)}>
            <div className="container">
              <div className="main-slide-text-wrapper-2">
                <div
                  className="main-slide-text-small"
                  style={{ paddingBottom: "0.5em" }}
                >
                  기업경쟁력과 함께 근로 생활의 질을 향상시킬 수 있는 사람중심의
                </div>
                <div
                  className="m_main-slide-text-small"
                  style={{ paddingBottom: "0.5em" }}
                >
                  기업경쟁력과 함께 근로 생활의 질을
                  <br />
                  향상시킬 수 있는 사람중심의
                </div>
                <div className="main-slide-text-title">Smart Factory</div>
              </div>
            </div>
          </div>
        </div>
        <div>
          <div style={slide1BackgroundStyle(backgroundImg3)}>
            <div className="container">
              <div className="main-slide-text-wrapper-3">
                <div className="main-slide-text-small">
                  최고의 기술력으로 제조기업에 최적화된
                </div>
                <div>
                  <img
                    src={objectImg3}
                    style={{
                      marginLeft: "2em",
                      marginRight: "2em",
                      width: "200px",
                    }}
                  ></img>
                </div>
                <div className="main-slide-text-small">
                  로 최상의 경쟁력을 갖출 수 있습니다.
                </div>
              </div>
            </div>
          </div>
        </div>
      </Slider>
    </div>
  )
}

export default MainSlide
