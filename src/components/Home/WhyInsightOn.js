import React from "react"

import "./whyInsightOn.css"
import InsightOnSpecial from "../Company/InsightOnSpecial"

const WhyInsightOn = () => (
  <>
    <section className={`home-why-section container`}>
      <div className="home-why-title">
        <h1 className="title">
          Why <span className="light">Choose</span> Insight ON
          <span className="light">?</span>
        </h1>
      </div>
      <InsightOnSpecial />
    </section>
  </>
)

export default WhyInsightOn
