import React from "react"
import h_square from "../images/h_square.jpg"

import "./verticalLine.css"

const VerticalLine = () => (
  <div className="vertical-line">
    <img alt="세로선" src={h_square} />
  </div>
)

export default VerticalLine
