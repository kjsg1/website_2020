import React from "react"
import h_square from "../images/h_square.jpg"

import "./verticalBottomLine.css"

const VerticalBottomLine = () => (
  <div className="vertical-bottom-line">
    <img alt="세로선" src={h_square} />
  </div>
)

export default VerticalBottomLine
