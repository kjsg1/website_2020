import React from "react"
import graph06 from "../../images/graph06.jpg"
import graph10 from "../../images/graph10.jpg"

import "./solution.css"

import VerticalLine from "../VerticalLine"
import HorizontalLine from "../HorizontalLine"
import HorizontalBottomLine from "../HorizontalBottomLine"
import SimpleInquiry from "../SimpleInquiry"

const MethodologyContainer = () => (
  <>
    <VerticalLine />

    <section className="solution-section-1">
      <h3 className="solution-section-title">AOM Methodology</h3>
      <div className="solution-section-2-content">
        인사이트온은{" "}
        <span className="bold red">스마트공장 객체 모델링 기법</span>과{" "}
        <span className="bold red">애자일 방법론</span>을 통합하여
        <br />
        인사이트온만의 <span className="bold">스마트공장 개발방법론</span>을
        정의하였고 <span className="bold">표준 프로젝트 수행 방법론</span>으로
        채택하였습니다.
        <br />
        <br />
        <span className="bold red">
        공정에 대한 높은 이해, 서비스의 조기 출시와 점진적 개선
      </span>으로 <span className="bold red">고객 만족</span>을 가능케 하는 비결.
        <br />
        바로 <span className="bold">AOM Methodology</span>입니다.
      </div>
      <div className="solution-section-2-img">
        <img alt="AOM Methodology 도식" src={graph06} />
      </div>
      <div className="m_solution-section-2-content">
        인사이트온은{" "}
        <span className="bold red">스마트공장 객체 모델링 기법</span>과{" "}
        <span className="bold red">애자일 방법론</span>을 통합하여
        인사이트온만의 <span className="bold">스마트공장 개발방법론</span>을
        정의하였고 <span className="bold">표준 프로젝트 수행 방법론</span>으로
        채택하였습니다.
        <span className="bold red">
          <br />
          <br />
          공정에 대한 높은 이해, 서비스의 조기 출시와 점진적 개선
        </span>
        으로 <span className="bold red">고객 만족</span>을 가능케 하는 비결. 바로{" "}
        <span className="bold">AOM Methodology</span>입니다.
      </div>
      <div className="m_solution-section-2-img">
        <img alt="AOM Methodology 도식" src={graph06} />
      </div>
    </section>

    <HorizontalLine />

    <section className="solution-section-2">
      <h3 className="solution-section-title">Object State and Activity Modeling</h3>
      <div className="solution-section-2-content">
        인사이트온만의 <span className="bold">스마트공장 모델링 방법론</span>
        <span className="light">
        (특허출원: 가공품 상태 전이도 모델링을 통한 MES API 자동화 방안)</span>
        은
        <span className="bold">
        신설공장이나 사용자의 요구사항이 극단적으로 적은 환경
        </span>
        에서 초기 설계에 방향성을 수립할 수 있는 도구입니다.
        <br />
        <br />
        인사이트온은 <span className="bold red">스마트공장 모델링 방법론</span>을 통해 생산과정을 도식화하고 분석함으로써
        <span className="bold">
          프로젝트 내 모든 인력의 공정에 대한 이해수준을 고객 수준 이상으로
        높이는 데 주력하고 있습니다.
        </span>
      </div>
      <div className="solution-section-2-img">
        <img alt="AOM Methodology 도식" src={graph10} />
      </div>
      <div className="m_solution-section-2-content">
        인사이트온만의 <span className="bold">스마트공장 모델링 방법론</span>
        <span className="light">
          (특허출원: 가공품 상태 전이도 모델링을 통한 MES API 자동화 방안)
        </span>
        은
        <span className="bold">
          신설공장이나 사용자의 요구사항이 극단적으로 적은 환경
        </span>
        에서 초기 설계에 방향성을 수립할 수 있는 도구입니다.
        <br />
        <br />
        인사이트온은 <span className="bold red">스마트공장 모델링 방법론</span>
        을 통해 생산과정을 도식화하고 분석함으로써
        <span className="bold">
          프로젝트 내 모든 인력의 공정에 대한 이해수준을 고객 수준 이상으로
          높이고 있습니다.
        </span>
      </div>
      <div className="m_solution-section-2-img">
        <img alt="AOM Methodology 도식" src={graph10} />
      </div>
    </section>

    <VerticalLine />

    <SimpleInquiry />

    <HorizontalBottomLine />
  </>
)

export default MethodologyContainer
