import React from "react"
import contimg09 from "../../images/contimg09.jpg"
import graph07 from "../../images/graph07.jpg"

import "./solution.css"

import VerticalLine from "../VerticalLine"
import VerticalBottomLine from "../VerticalBottomLine"
import HorizontalLine from "../HorizontalLine"
import SimpleInquiry from "../SimpleInquiry"
import Projects from "./Projects/AutomationProjects"

const AutomationContainer = () => (
  <>
    <VerticalLine />

    <section className="solution-section-1">
      <h3 className="solution-section-title">
        생산수단의 기계화와 자동화, 그리고 MES와의 연동
      </h3>
      <div className="solution-section-1-wrapper">
        <div className="solution-section-1-img">
          <img alt="배경이미지" src={contimg09} />
        </div>
        <div className="solution-section-1-content">
          노동 집약의 제조현장을 생산 수단의 기계화 및 자동화를 통해 생산성
          향상과 제조원가의 절감이 가능합니다. 장비들은 범용적으로
          PLC(Programmable Logic Controller)에 의해 제어되고, HMI(Human Machine
          Interface) 시스템을 통해 설비통제 시나리오를 구축하고 기간계 시스템과
          연동할 수 있습니다.
          <br />
          <br />
          스마트팩토리 구축을 위하여 자동화 설비와 기간계 시스템 연동의 중요성은
          날로 높아지고 있으나, 현장에서는 연동에 많은 시간과 노력을 투입하고도,
          큰 시행착오를 겪는 문제점이 있습니다.
        </div>
      </div>
      <div className="m_solution-section-1-content">
        노동 집약의 제조현장을 생산 수단의 기계화 및 자동화를 통해 생산성 향상과
        제조원가의 절감이 가능합니다.
        <br />
        <br />
        장비들은 범용적으로 PLC(Programmable Logic Controller)에 의해 제어되고,
        HMI(Human Machine Interface) 시스템을 통해 설비통제 시나리오를 구축하고
        기간계 시스템과 연동할 수 있습니다.
        <br />
        <br />
        스마트팩토리 구축을 위하여 자동화 설비와 기간계 시스템 연동의 중요성은
        날로 높아지고 있으나, 현장에서는 연동에 많은 시간과 노력을 투입하고도,
        큰 시행착오를 겪는 문제점이 있습니다.
      </div>
    </section>

    <HorizontalLine />

    <section className="solution-section-2">
      <h3 className="solution-section-title">NFS Pbot (PLC bot)</h3>
      <div className="solution-section-2-content">
        Smart Factory 구축에 최대의 장애요소인 자동화 설비와 MES 연동 문제를
        해결하기 위한 솔루션입니다.
        <br />
        <br />
        스마트팩토리의 신규 구축, 라인변경, 증설에 따른 자동화 설비와 기간계
        연계 시 시행착오를 줄이기 위하여 프로그램 개발방식을 Workflow 다이어그램
        작성을 통한 로직 자동완성 방식으로 변경하고, 설비 셋업 전에도
        시뮬레이션(테스트)할 수 있는 설비통신 응용프로그램을 개발할 수
        있습니다. 완성된 시나리오는 실제 설비통신을 담당하는 PbotEngine을
        사용하여 양방향으로 정보전송, 통신상태 모니터링, 테스트 케이스에 대한
        송수신 테스트가 가능하며, 저장된 Rule의 인위적인 조작을 통해
        스마트팩토리의 장애, 사고 등 비정상적인 상황을 사전에 시뮬레이션할 수
        있는 환경을 제공합니다.
        <br />
        <br />
        다양한 설비 프로토콜과 산업표준(OPC, PLC 등)을 지원합니다.
      </div>
      <div className="solution-section-2-img">
        <img alt="솔루션 도식" src={graph07} />
      </div>
      <div className="m_solution-section-2-content">
        Smart Factory 구축에 최대의 장애요소인 자동화 설비와 MES 연동 문제를
        해결하기 위한 솔루션입니다.
        <br />
        <br />
        스마트팩토리의 신규 구축, 라인변경, 증설에 따른 자동화 설비와 기간계
        연계 시 시행착오를 줄이기 위하여 프로그램 개발방식을 Workflow 다이어그램
        작성을 통한 로직 자동완성 방식으로 변경하고, 설비 셋업 전에도
        시뮬레이션(테스트)할 수 있는 설비통신 응용프로그램을 개발할 수
        있습니다.
        <br />
        <br />
        완성된 시나리오는 실제 설비통신을 담당하는 PbotEngine을 사용하여
        양방향으로 정보전송, 통신상태 모니터링, 테스트 케이스에 대한 송수신
        테스트가 가능하며, 저장된 Rule의 인위적인 조작을 통해 스마트팩토리의
        장애, 사고 등 비정상적인 상황을 사전에 시뮬레이션할 수 있는 환경을
        제공합니다.
        <br />
        <br />
        다양한 설비 프로토콜과 산업표준(OPC, PLC 등)을 지원합니다.
      </div>
      <div className="m_solution-section-2-img">
        <img alt="솔루션 도식" src={graph07} />
      </div>
    </section>

    <VerticalLine />

    <Projects />

    <HorizontalLine />

    <SimpleInquiry />

    <VerticalBottomLine />
  </>
)

export default AutomationContainer
