import React from "react"
import contimg12 from "../../images/contimg12.jpg"
import contimg14 from "../../images/contimg14.jpg"
import graph09 from "../../images/graph09.jpg"

import "./solution.css"

import VerticalLine from "../VerticalLine"
import VerticalBottomLine from "../VerticalBottomLine"
import HorizontalLine from "../HorizontalLine"
import SimpleInquiry from "../SimpleInquiry"
import Projects from "./Projects/QualityProjects"

const QualityContainer = () => (
  <>
    <VerticalLine />

    <section className="solution-section-1">
      <h3 className="solution-section-title">
        생산 효율성을 높이기 위한 열쇠 "품질관리시스템"
      </h3>
      <div className="solution-section-1-wrapper">
        <div className="solution-section-1-img">
          <img alt="배경이미지" src={contimg12} />
        </div>
        <div className="solution-section-1-content">
          제조 공정 관점에서의 품질관리는 생산 공정 전/후와 공정 중에 재료 및
          제품의 품질을 측정하고 분석하여 불량을 줄이고 수율을 높임으로써,
          생산성을 향상하는 것을 목표로 합니다.
          <br />
          <br />
          점차 복잡해지는 생산환경으로 인해 품질관리시스템을 활용한 품질 데이터
          분석과 추적의
          <br />
          필요성이 증가하고 있습니다. 특히 초정밀 산업(반도체, 전자,
          배터리)에서는 품질관리를 위한
          <br />
          측정공정이 20~30%를 차지하고 있으며 그 비중이 계속 증가하고 있습니다.
        </div>
      </div>
      <div className="m_solution-section-1-content">
        제조 공정 관점에서의 품질관리는 생산 공정 전/후와 공정 중에 재료 및
        제품의 품질을 측정하고 분석하여 불량을 줄이고 수율을 높임으로써,
        생산성을 향상하는 것을 목표로 합니다.
        <br />
        <br />
        점차 복잡해지는 생산환경으로 인해 품질관리시스템을 활용한 품질 데이터
        분석과 추적의 필요성이 증가하고 있습니다.
        <br />
        <br />
        특히 초정밀 산업(반도체, 전자, 배터리)에서는 품질관리를 위한 측정공정이
        20~30%를 차지하고 있으며 그 비중이 계속 증가하고 있습니다.
      </div>
    </section>

    <section className="solution-section-1">
      <h3 className="solution-section-title">
        고객의 요구사항을 관리하여 고객 만족을 실현하는 "품질경영시스템"
      </h3>
      <div className="solution-section-1-wrapper">
        <div className="solution-section-1-img">
          <img alt="배경이미지" src={contimg14} />
        </div>
        <div className="solution-section-1-content">
          생산공정 이후에도 제품 및 서비스의 품질보증, 제품 책임에 대한
          이해관계자들의 요구를
          <br />
          품질경영시스템을 통해 관리하고, 제품의 개선에 반영하여 고객 만족을
          도모하는 것을 목표로 합니다.
        </div>
      </div>
      <div className="m_solution-section-1-content">
        생산공정 이후에도 제품 및 서비스의 품질보증, 제품 책임에 대한
        이해관계자들의 요구를 품질경영시스템을 통해 관리하고, 제품의 개선에
        반영하여 고객만족을 도모하는 것을 목표로 합니다.
      </div>
    </section>

    <HorizontalLine />

    <section className="solution-section-2">
      <h3 className="solution-section-title">Lab OneQ</h3>
      <div className="solution-section-2-content">
        클라우드 기반의 구독형 LIMS(실험정보관리시스템) 화학제조, 제약, 식품제조
        산업의 품질관리를 위한 솔루션입니다.
      </div>
      <div className="solution-section-2-img">
        <img alt="Lab OneQ 도식" src={graph09} />
      </div>
      <div className="m_solution-section-2-content">
        클라우드 기반의 구독형 LIMS(실험정보관리시스템) 화학제조, 제약, 식품제조
        산업의 품질관리를 위한 솔루션입니다.
      </div>
      <div className="m_solution-section-2-img">
        <img alt="Lab OneQ 도식" src={graph09} />
      </div>
    </section>

    <VerticalLine />

    <Projects />

    <HorizontalLine />

    <SimpleInquiry />

    <VerticalBottomLine />
  </>
)

export default QualityContainer
