import React from "react"
import contimg10 from "../../images/contimg10.jpg"
import graph01 from "../../images/graph01.jpg"

import "./solution.css"

import VerticalLine from "../../components/VerticalLine"
import VerticalBottomLine from "../VerticalBottomLine"
import HorizontalLine from "../../components/HorizontalLine"
import SimpleInquiry from "../SimpleInquiry"
import Projects from "./Projects/OperationProjects"

const OperationContainer = () => (
  <>
    <VerticalLine />

    <section className="solution-section-1">
      <h3 className="solution-section-title">
        스마트 공장 시스템의 핵심인 "통합생산관리시스템" MES
      </h3>
      <div className="solution-section-1-wrapper">
        <div className="solution-section-1-img">
          <img alt="배경이미지" src={contimg10} />
        </div>
        <div className="solution-section-1-content">
          공정진행 정보의 모니터링과 통제, 설비제어, 품질정보관리 등{" "}
          <span className="bold">
            <br />
            생산 현장에서 발생할 수 있는 모든 정보를 통합 관리
          </span>
          하는 시스템입니다.
          <br />
          <br />
          실시간으로 상황을 파악하고 대응할 수 있도록
          <span className="bold"> 제조정보의 가시성</span>과 <br />
          생산진척률, 불량/사고 분석을 위한{" "}
          <span className="bold">추적 관리</span>가 주요한 시스템입니다.
        </div>
      </div>
      <div className="m_solution-section-1-content">
        공정진행 정보의 모니터링과 통제, 설비제어, 품질정보관리 등{" "}
        <span className="bold">
          생산 현장에서 발생할 수 있는 모든 정보를 통합 관리
        </span>
        하는 시스템입니다.
        <br />
        <br />
        실시간으로 상황을 파악하고 대응할 수 있도록
        <span className="bold"> 제조정보의 가시성</span>과 생산진척률, 불량/사고
        분석을 위한 <span className="bold">추적 관리</span>가 주요한
        시스템입니다.
      </div>
    </section>

    <HorizontalLine />

    <section className="solution-section-2">
      <h3 className="solution-section-title">NFS MES</h3>
      <div className="solution-section-2-content">
        인사이트온의 MES 솔루션으로 제조 효율성과 생산성을 극대화하세요.
        <br />
        <br />
        Beacon 등 IoT 기술을 이용해{" "}
        <span className="bold">Smart 대차와 Kanban</span>을 제공하며,
        <br />
        이를 활용하여{" "}
        <span className="bold">
          현장의 생산진행 현황을 실시간으로 통제 및 모니터링
        </span>{" "}
        할 수 있습니다.
      </div>
      <div className="solution-section-2-img">
        <img alt="nfsmes 도식" src={graph01} />
      </div>

      <div className="m_solution-section-2-content">
        인사이트온의 MES 솔루션으로 제조 효율성과 생산성을 극대화하세요.
        <br />
        <br />
        Beacon 등 IoT 기술을 이용해{" "}
        <span className="bold">Smart 대차와 Kanban</span>을 제공하며, 이를
        활용하여{" "}
        <span className="bold">
          현장의 생산진행 현황을 실시간으로 통제 및 모니터링
        </span>{" "}
        할 수 있습니다.
      </div>
      <div className="m_solution-section-2-img">
        <img alt="nfsmes 도식" src={graph01} />
      </div>
    </section>

    <VerticalLine />

    <Projects />

    <HorizontalLine />

    <SimpleInquiry />

    <VerticalBottomLine />
  </>
)

export default OperationContainer
