import React from "react"
import contimg13 from "../../images/contimg13.jpg"

import "./solution.css"

import VerticalLine from "../VerticalLine"
import HorizontalBottomLine from "../HorizontalBottomLine"
import HorizontalLine from "../HorizontalLine"
import SimpleInquiry from "../SimpleInquiry"
import Projects from "./Projects/EquipmentProjects"

const EquipmentContainer = () => (
  <>
    <VerticalLine />

    <section className="solution-section-1">
      <h3 className="solution-section-title">
        설비자산 보전 활동의 전 과정을 관리할 수 있는 시스템
      </h3>
      <div className="solution-section-1-wrapper">
        <div className="solution-section-1-img">
          <img alt="배경이미지" src={contimg13} />
        </div>
        <div className="solution-section-1-content">
          설비 관리 시스템은 주로 대규모 플랜트, 제조 공장 등 대규모 사업장에서
          주로 도입되었으나, 최근에는 중소 규모의 공장에도 도입 사례가 늘고 있습니다.
          <br />
          <br />
          새로운 기술 개발과 지능화된 설비의 등장으로 설비운영의 불확실성과
          복잡도가 증가하여 관리자의 경험적 지식과 수기, 인력에 기대고 있던 관리 방식은 한계에
          다다랐습니다.
          <br />
          <br />
          이에 따라 시스템으로 이력을 관리, 분석하고 통제할 수 있는 최적의 유지
          보수 전략이 필요하게 되었습니다.
          
          <br />
          설비관리 시스템으로 스마트한 설비 정비와 진단, 자산보전 체계를 구축할
          수 있습니다.
        </div>
      </div>
      <div className="m_solution-section-1-content">
        설비 관리 시스템은 주로 대규모 플랜트, 제조 공장 등 대규모 사업장에서
        주로 도입되었으나, 최근에는 중소 규모의 공장에도 도입 사례가 늘고
        있습니다.
        <br />
        <br />
        새로운 기술 개발과 지능화된 설비의 등장으로 설비운영의 불확실성과
        복잡도가 증가하여 관리자의 경험적 지식과 수기, 인력에 기대고 있던 관리
        방식은 한계에 다다랐습니다.
        <br />
        <br />
        이에 따라 시스템으로 이력을 관리, 분석하고 통제할 수 있는 최적의 유지
        보수 전략이 필요하게 되었습니다. 설비관리 시스템으로 스마트한 설비
        정비와 진단, 자산보전 체계를 구축할 수 있습니다.
      </div>
    </section>
    <HorizontalLine />

    <Projects />

    <VerticalLine />

    <SimpleInquiry />

    <HorizontalBottomLine />
  </>
)

export default EquipmentContainer
