import React from "react"

const LogisticsProjects = () => (
  <>
    <section className="projects-section">
      <h3 className="projects-section-title">구축사례</h3>
      <div className="constructionbgbox01">
        <div className="constructionbox1">
          <div className="constructionbox3">
            <div className="constructioncontbox01">
              <h2 className="contructitle">2021</h2>
              <p className="contrucp">
                <span className="bold">· 금호석유화학</span> : WMS 구축
              </p>
            </div>
            <div className="constructioncontbox02">
              <h2 className="contructitle">2019</h2>
              <p className="contrucp">
                <span className="bold">· SK IET</span> : FCW 공장 생산관리 및
                외주 임가공 시스템 구축
                <br />
                <span className="bold">· SK IET</span> : 글로벌 표준 IT 시스템
                구축
                <br />
                <span className="bold">· NEXFLEX</span> : 연성회로기판 MES
                시스템 유지보수
                <br />
                <span className="bold">· 에센코어 반도체</span> : SCM 시스템
                유지보수
              </p>
            </div>
          </div>

          <div className="constructionbox3">
            <div className="constructioncontbox03">
              <h2 className="contructitle">2018</h2>
              <p className="contrucp">
                <span className="bold">· 에센코어 반도체</span> : 공장 Smart
                Factory MES 구축
                <br />
                <span className="bold">· LG화학</span> : 차세대 생산관리 시스템
                PI컨설팅
              </p>
            </div>
            <div className="constructioncontbox04">
              <h2 className="contructitle">2015-2017</h2>
              <p className="contrucp">
                <span className="bold">· SK이노베이션</span> : 배터리 화성 공장
                MES 및 ECS 구축
                <br />
                <span className="bold">· SK이노베이션</span> : 정보전자 소재 WMS
                시스템 구축
                <br />
                <span className="bold">· CJ바이오</span> : Global MES 구축
                <br />
                <span className="bold">· 창신INC</span> : NIKE 공장 Global MES
                1차 구축
                <br />
                <span className="bold">· 창신INC</span> : NIKE 공장 Global MES
                표준화 및 Roadmap PI
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section className="m_projects-section">
      <h3 className="projects-section-title">구축사례</h3>
      <div className="m_constructionbgbox">
        <div className="m_construction-content">
          <div>
            <div className="construction-content-title">2021</div>
            <div>
              <p className="construction-content-text">
                <span className="bold">· 금호석유화학</span> : WMS 구축
              </p>
            </div>
          </div>
        </div>
        <div className="m_construction-content">
          <div>
            <div className="construction-content-title">2019</div>
            <div>
              <p className="construction-content-text">
                <span className="bold">· SK IET</span> : FCW 공장 생산관리 및
                외주 임가공 시스템 구축
                <br />
                <span className="bold">· SK IET</span> : 글로벌 표준 IT 시스템
                구축
                <br />
                <span className="bold">· NEXFLEX</span> : 연성회로기판 MES
                시스템 유지보수
                <br />
                <span className="bold">· 에센코어 반도체</span> : SCM 시스템
                유지보수
              </p>
            </div>
          </div>
        </div>
        <div className="m_construction-content">
          <div>
            <div className="construction-content-title1">2018</div>
            <div>
              <p className="construction-content-text">
                <span className="bold">· 에센코어 반도체</span> : 공장 Smart
                Factory MES 구축
                <br />
                <span className="bold">· LG화학</span> : 차세대 생산관리 시스템
                PI컨설팅{" "}
              </p>
            </div>
          </div>
        </div>
        <div className="m_construction-content">
          <div>
            <div className="construction-content-title1">2017</div>
            <div>
              <p className="construction-content-text">
                <span className="bold">· SK이노베이션</span> : 배터리 화성 공장
                MES 및 ECS 구축
                <br />
                <span className="bold">· SK이노베이션</span> : 정보전자 소재 WMS
                시스템 구축
                <br />
                <span className="bold">· CJ바이오</span> : Global MES 구축
              </p>
            </div>
          </div>
        </div>
        <div className="m_construction-content1">
          <div>
            <h2 className="construction-content-title1">2015-2016</h2>
            <p className="construction-content-text1">
              <span className="bold">· 창신INC</span> : NIKE 공장 Global MES 1차
              구축
              <br />
              <span className="bold">· 창신INC</span> : NIKE 공장 Global MES
              표준화 및 Roadmap PI{" "}
            </p>
          </div>
        </div>
      </div>
    </section>
  </>
)

export default LogisticsProjects
