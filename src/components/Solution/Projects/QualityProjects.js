import React from "react"

const QualityProjects = () => (
  <>
    <section className="projects-section">
      <h3 className="projects-section-title">구축사례</h3>
      <div className="constructionbgbox01">
        <div className="constructionbox1">
          <div className="constructionbox4">
            <div className="constructioncontbox01">
              <h2 className="contructitle">2020</h2>
              <p className="contrucp">
                <span className="bold">· 롯데정밀화학</span> :
                인천품질관리시스템 업그레이드
              </p>
            </div>
            <div className="constructioncontbox02">
              <h2 className="contructitle">2019</h2>
              <p className="contrucp">
                <span className="bold">· SK IET</span> : FCW 공장 생산관리 및
                외주 임가공 시스템 구축
                <br />
                <span className="bold">· SK IET</span> : 품질경영 시스템구축
                <br />
                <span className="bold">· SK이노베이션</span> : 충방전 설비관리
                시스템 구축
                <br />
                <span className="bold">· SK IET</span> : 글로벌 표준 IT 시스템
                구축
                <br />
                <span className="bold">· NEXFLEX</span> : 연성회로기판 MES
                시스템 유지보수
                <br />
                <span className="bold">· LG화학</span> : 차세대 생산/품질관리
                시스템 구축
              </p>
            </div>
          </div>

          <div className="constructionbox3">
            <div className="constructioncontbox03">
              <h2 className="contructitle">2018</h2>
              <p className="contrucp">
                <span className="bold">· LG화학</span> : 차세대 생산관리 시스템
                PI컨설팅
                <br />
                <span className="bold">· SK이노베이션</span> : 차세대 OIS
                생산관리 시스템 구축
              </p>
            </div>
            <div className="constructioncontbox04">
              <h2 className="contructitle">2017</h2>
              <p className="contrucp">
                <span className="bold">· SK이노베이션</span> : 배터리 화성 공장
                MES 및 ECS 구축
                <br />
                <span className="bold">· SK이노베이션</span> : 차세대 OIS
                생산관리 시스템 To-Be 설계
                <br />
                <span className="bold">· CJ바이오</span> : Global MES 구축
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section className="m_projects-section">
      <h3 className="projects-section-title">구축사례</h3>
      <div className="m_constructionbgbox">
        <div className="m_construction-content">
          <div>
            <div className="construction-content-title">2020</div>
            <div>
              <p className="construction-content-text">
                <span className="bold">· 롯데정밀화학</span> :
                인천품질관리시스템 업그레이드
              </p>
            </div>
          </div>
        </div>
        <div className="m_construction-content">
          <div>
            <div className="construction-content-title1">2019</div>
            <div>
              <p className="construction-content-text">
                <span className="bold">· SK IET</span> : FCW 공장 생산관리 및
                외주 임가공 시스템 구축
                <br />
                <span className="bold">· SK IET</span> : 품질경영 시스템구축
                <br />
                <span className="bold">· SK이노베이션</span> : 충방전 설비관리
                시스템 구축
                <br />
                <span className="bold">· SK IET</span> : 글로벌 표준 IT 시스템
                구축
                <br />
                <span className="bold">· NEXFLEX</span> : 연성회로기판 MES
                시스템 유지보수
                <br />
                <span className="bold">· LG화학</span> : 차세대 생산/품질관리
                시스템 구축
              </p>
            </div>
          </div>
        </div>
        <div className="m_construction-content">
          <div>
            <div className="construction-content-title1">2018</div>
            <div>
              <p className="construction-content-text">
                <span className="bold">· LG화학</span> : 차세대 생산관리 시스템
                PI컨설팅
                <br />
                <span className="bold">· SK이노베이션</span> : 차세대 OIS
                생산관리 시스템 구축
              </p>
            </div>
          </div>
        </div>
        <div className="m_construction-content1">
          <div>
            <h2 className="construction-content-title1">2017</h2>
            <p className="construction-content-text1">
              <span className="bold">· SK이노베이션</span> : 배터리 화성 공장
              MES 및 ECS 구축
              <br />
              <span className="bold">· SK이노베이션</span> : 차세대 OIS 생산관리
              시스템 To-Be 설계
              <br />
              <span className="bold">· CJ바이오</span> : Global MES 구축
            </p>
          </div>
        </div>
      </div>
    </section>
  </>
)

export default QualityProjects
