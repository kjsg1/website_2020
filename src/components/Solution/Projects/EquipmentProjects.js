import React from "react"

import "./projects.css"

const EquipmentProjects = () => (
  <>
    <section className="projects-section">
      <h3 className="projects-section-title">구축사례</h3>
      <div className="constructionbgbox02">
        <div className="constructionbox6">
          <div className="constructionbox7">
            <div className="constructioncontbox05">
              <h2 className="contructitle">2019</h2>
              <p className="contrucp">
                <span className="bold"> · SK이노베이션</span> : 차세대 설비관리
                시스템 구축
                <br />
                <span className="bold"> · SK IET</span> : 설비관리 시스템 구축
                <br />
                <span className="bold"> · SK이노베이션</span> : 충방전 설비관리
                시스템 구축
                <br />
                <span className="bold"> · SK IET</span> : 글로벌 표준 IT 시스템
                구축
              </p>
            </div>
            <div className="constructioncontbox06">
              <h2 className="contructitle">2017</h2>
              <p className="contrucp">
                <span className="bold"> · SK이노베이션</span> : 배터리 화성 공장
                MES 및 ECS 구축
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section className="m_projects-section">
      <h3 className="projects-section-title">구축사례</h3>
      <div className="m_constructionbgbox">
        <div className="m_construction-content">
          <div>
            <div className="construction-content-title">2019</div>
            <div>
              <p className="construction-content-text">
                <span className="bold"> · SK이노베이션</span> : 차세대 설비관리
                시스템 구축
                <br />
                <span className="bold"> · SK IET</span> : 설비관리 시스템 구축
                <br />
                <span className="bold"> · SK이노베이션</span> : 충방전 설비관리
                시스템 구축
                <br />
                <span className="bold"> · SK IET</span> : 글로벌 표준 IT 시스템
                구축
              </p>
            </div>
          </div>
        </div>
        <div className="m_construction-content1">
          <div>
            <h2 className="construction-content-title1">2017</h2>
            <p className="construction-content-text1">
              <span className="bold"> · SK이노베이션</span> : 배터리 화성 공장
              MES 및 ECS 구축
            </p>
          </div>
        </div>
      </div>
    </section>
  </>
)

export default EquipmentProjects
