import React from "react"
import contimg11 from "../../images/contimg11.jpg"
import graph02 from "../../images/graph02.jpg"

import "./solution.css"

import VerticalLine from "../VerticalLine"
import VerticalBottomLine from "../VerticalBottomLine"
import HorizontalLine from "../HorizontalLine"
import SimpleInquiry from "../SimpleInquiry"
import Projects from "./Projects/LogisticsProjects"

const LogisticsContainer = () => (
  <>
    <VerticalLine />

    <section className="solution-section-1">
      <h3 className="solution-section-title">
        제조 비용 절감과 효율화를 위한 필수 시스템,
        <br />
        "재고관리시스템"과 "물류관리시스템"
      </h3>
      <div className="solution-section-1-wrapper">
        <div className="solution-section-1-img">
          <img alt="배경이미지" src={contimg11} />
        </div>
        <div className="solution-section-1-content">
          제조과정에서는 크게 원자재의 조달에서 입하까지의{" "}
          <span className="bold">조달 물류</span>와 <br />
          자재창고 출고에서부터 생산공정으로의 운반 및 제품 창고에의 입고까지의
          <span className="bold"> 생산물류</span>, <br />
          제품의 포장과 출하, 판매까지의 <span className="bold">제품 물류</span>
          로 구분됩니다.
          <span className="bold">
            <br />
            <br />
            생산관리, 생산계획, 판매계획과 필연적으로 연동
          </span>
          되어야 하며,{" "}
          <span className="bold">
            <br />
            물류비용과 창고 운영비용, 인건비, 그리고 재고비용 절감
          </span>
          에 기여하는 중요한 시스템입니다.
        </div>
      </div>
      <div className="m_solution-section-1-content">
        제조과정에서는 크게 원자재의 조달에서 입하까지의{" "}
        <span className="bold">조달 물류</span>와 자재창고 출고에서부터
        생산공정으로의 운반 및 제품 창고에의 입고까지의
        <span className="bold"> 생산물류</span>, 제품의 포장과 출하, 판매까지의{" "}
        <span className="bold">제품 물류</span>로 구분됩니다.
        <br />
        <br />
        <span className="bold">
          생산관리, 생산계획, 판매계획과 필연적으로 연동
        </span>
        되어야 하며,{" "}
        <span className="bold">
          물류비용과 창고 운영비용, 인건비, 그리고 재고비용 절감
        </span>
        에 기여하는 중요한 시스템입니다.
      </div>
    </section>

    <HorizontalLine />

    <section className="solution-section-2">
      <h3 className="solution-section-title">NFS WMS</h3>
      <div className="solution-section-2-content">
        loT 기술, 스마트 기기를 통해 위치 정보를 활용한{" "}
        <span className="bold">Smart 재고 실사</span>
        기능을 포함하여
        <br />
        입고, 적차, 재고, 피킹, 출고 등{" "}
        <span className="bold">물류센터 프로세스 전체를 통합 관리</span>하는
        솔루션입니다.
      </div>
      <div className="solution-section-2-img">
        <img alt="솔루션 도식" src={graph02} />
      </div>

      <div className="m_solution-section-2-content">
        loT 기술, 스마트 기기를 통해 위치 정보를 활용한{" "}
        <span className="bold">Smart 재고 실사</span>
        기능을 포함하여
        <br />
        입고, 적차, 재고, 피킹, 출고 등{" "}
        <span className="bold">물류센터 프로세스 전체를 통합 관리</span>하는
        솔루션입니다.
      </div>
      <div className="m_solution-section-2-img">
        <img alt="솔루션 도식" src={graph02} />
      </div>
    </section>

    <VerticalLine />

    <Projects />

    <HorizontalLine />

    <SimpleInquiry />

    <VerticalBottomLine />
  </>
)

export default LogisticsContainer
