import React from "react"

import VerticalLine from "../VerticalLine"
import HorizontalLine from "../HorizontalLine"
import HorizontalBottomLine from "../HorizontalBottomLine"
import SimpleInquiry from "../SimpleInquiry"

import Reference from '../Home/Reference'

import "./accomplishment.css"

import AccomplishmentTabArea from "./AccomplishmentTabArea"

const AccomplishmentContainer = () => (
  <>
    <VerticalLine />

    <Reference isInMainPage={false} />

    <HorizontalLine />

    <AccomplishmentTabArea />

    <VerticalLine />

    <SimpleInquiry />

    <HorizontalBottomLine />
  </>
)

export default AccomplishmentContainer
