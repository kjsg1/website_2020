import React, { useState } from "react"

import "./accomplishmentTabArea.css"

const labelStyle = selected => {
  if (selected) {
    return {
      borderBottom: "4px solid #ff0000",
    }
  }
  return {}
}

// DT
const TabContent1 = () => (
  <div className="accomplishment-tab-content-container">
    <div className="accomplishment-tab-content">
      <div className="accomplishment-tab-content-title">2021</div>
      <div className="accomplishment-tab-content-text">
        <ul>
          <li>
            <span className="bold">· 울산정보산업진흥원</span> : 중소 상생형 데이터·AI융합 기획연구
(설비관리 중심으로)
            <br/><span className="tag">IoT</span><span className="tag">클라우드</span><span className="tag">머신러닝</span>
          </li>
          <li>
            <span className="bold">· 광물자원공사</span> : 광산 Digital Mining 플랫폼 구축 (2차)
            <br/><span className="tag">IoT</span><span className="tag">클라우드</span>
          </li>
        </ul>
      </div>
    </div>
    <div className="accomplishment-tab-content">
      <div className="accomplishment-tab-content-title">2020</div>
      <div className="accomplishment-tab-content-text">
        <ul>
          <li>
            <span className="bold">· 광물자원공사</span> : 광산 Digital Mining 플랫폼 구축 (1차)
            <br/><span className="tag">IoT</span><span className="tag">클라우드</span>
          </li>
          <li>
            <span className="bold">· 중소벤처기업부</span> : 블록체인 기반 시험정보 플랫폼 구축
            <br/><span className="tag">클라우드</span><span className="tag">블록체인</span>
          </li>
          <li>
            <span className="bold">· 울산경제진흥원</span> : SI 전문 프리랜서 매칭 플랫폼 구축 고도화
            <br/><span className="tag">클라우드</span><span className="tag">블록체인</span>
          </li>
        </ul>
      </div>
    </div>
    <div className="accomplishment-tab-content">
      <div className="accomplishment-tab-content-title">2019</div>
      <div className="accomplishment-tab-content-text">
        <ul>
          <li>
            <span className="bold">· 중소벤처기업부</span> : Rule 기반 설비통신 자동화 플랫폼 구축
            <br/><span className="tag">클라우드</span>
          </li>
          <li>
            <span className="bold">· 자사 프로젝트</span> : SI 전문 프리랜서 매칭 플랫폼 MVP 개발
            <br/><span className="tag">클라우드</span>
          </li>
        </ul>
      </div>
    </div>
  </div>
)

//석유화학
const TabContent2 = () => (
  <div className="accomplishment-tab-content-container">
    <div className="accomplishment-tab-content">
      <div className="accomplishment-tab-content-title">2020</div>
      <div className="accomplishment-tab-content-text">
        <ul>
          <li>
            <span className="bold">· SK이노베이션</span> : SKI 차세대 설비관리 시스템 구축
            <span className="tag">설비</span>
          </li>
          <li>
            <span className="bold">· SK에너지</span> : SK 항무관리 시스템 개발
            <span className="tag">생산</span>
          </li>
          <li>
            <span className="bold">· 광물자원공사</span> : Digital Mining 플랫폼 구축
            <span className="tag">생산</span><span className="tag">설비</span>
          </li>
          <li>
            <span className="bold">· SKIET</span> : MES/WMS/SPC/QMS 시스템 구축
            <br/><span className="tag">생산</span><span className="tag">설비</span><span className="tag">물류</span><span className="tag">품질</span>
          </li>
          <li>
            <span className="bold">· SKIET</span> : SKIET 제안관리시스템 구축
            <span className="tag">품질</span>
          </li>
          <li>
            <span className="bold">· SKIET</span> : 중국  MES/WMS/SPC/QMS 시스템 구축
            <br/><span className="tag">생산</span><span className="tag">설비</span><span className="tag">물류</span><span className="tag">품질</span>
          </li>
          <li>
            <span className="bold">· SKIET</span> : 폴란드 MES/WMS/SPC/QMS  구축
            <br/><span className="tag">생산</span><span className="tag">설비</span><span className="tag">물류</span><span className="tag">품질</span>
          </li>
          <li>
            <span className="bold">· SKI</span> : 헝가리 MES/WMS/QMS/SCM  구축
            <br/><span className="tag">생산</span><span className="tag">설비</span><span className="tag">물류</span><span className="tag">품질</span>
          </li>
          <li>
            <span className="bold">· 현대중공업</span> : 특수선 중일정 관리 시스템 구축
            <span className="tag">생산</span>
          </li>
          <li>
            <span className="bold">· SK에너지</span> : 화학물질관리시스템 재개발
            <span className="tag">생산</span>
          </li>
          <li>
            <span className="bold">· 인사이트온</span> : 통합 바코드 시스템 구축
            <span className="tag">물류</span>
          </li>
          <li>
            <span className="bold">· 롯데정밀화학</span> : 롯데정밀화학 LIMS 시스템 구축
            <span className="tag">품질</span>
          </li>
        </ul>
      </div>
    </div>
    <div className="accomplishment-tab-content">
      <div className="accomplishment-tab-content-title">2019</div>
      <div className="accomplishment-tab-content-text">
        <ul>
          <li>
            <span className="bold">· SK이노베이션</span> : SKI 차세대 설비관리 시스템 구축
            <span className="tag">설비</span>
          </li>
          <li>
            <span className="bold">· LG 화학</span> : LG화학 차세대 생산/품질관리 시스템 구축
            <br/><span className="tag">생산</span><span className="tag">품질</span>
          </li>
        </ul>
      </div>
    </div>

    <div className="accomplishment-tab-content">
      <div className="accomplishment-tab-content-title">2018</div>
      <div className="accomplishment-tab-content-text">
        <ul>
          <li>
            <span className="bold">· LG 화학</span> : LG화학 차세대 생산관리 시스템 PI컨설팅
            <br/><span className="tag">생산</span><span className="tag">물류</span><span className="tag">품질</span>
          </li>
          <li>
            <span className="bold">· SK이노베이션</span> : Digital Transformation 시스템 구축
            <br/><span className="tag">생산</span>
          </li>
          <li>
            <span className="bold">· SK이노베이션</span> : 차세대 OIS 생산관리 시스템 구축
            <br/><span className="tag">생산</span><span className="tag">품질</span>
          </li>
        </ul>
      </div>
    </div>

    <div className="accomplishment-tab-content">
      <div className="accomplishment-tab-content-title">2017</div>
      <div className="accomplishment-tab-content-text">
        <ul>
          <li>
            <span className="bold">· SK이노베이션</span> : 차세대 OIS 생산관리 시스템  To-Be 설계
            <br/><span className="tag">생산</span><span className="tag">품질</span>
          </li>
          <li>
            <span className="bold">· CJ 바이오</span> : CJ 바이오 Global MES 구축
            <br/><span className="tag">생산</span><span className="tag">물류</span><span className="tag">품질</span>
          </li>
        </ul>
      </div>
    </div>
  </div>
)

//반도체 및 일반제조
const TabContent3 = () => (
  <div className="accomplishment-tab-content-container">
    <div className="accomplishment-tab-content">
      <div className="accomplishment-tab-content-title">2020</div>
      <div className="accomplishment-tab-content-text">
        <ul>
          <li>
            <span className="bold">· SKI</span> : SKI 배터리 생산관리 시스템 구축(중국, 헝가리)
            <span className="tag">생산</span><span className="tag">설비</span><span className="tag">물류</span>
          </li>
          <li>
            <span className="bold">· SK IET</span> : SKIET 창저우 생산관리 시스템 구축
            <br/><span className="tag">생산</span><span className="tag">설비</span><span className="tag">물류</span>
          </li>
          <li>
            <span className="bold">· 인사이트온</span> : SKIET 제안관리 시스템 구축
            <span className="tag">품질</span>
          </li>
        </ul>
      </div>
    </div>
    <div className="accomplishment-tab-content">
      <div className="accomplishment-tab-content-title">2019</div>
      <div className="accomplishment-tab-content-text">
        <ul>
          <li>
            <span className="bold">· 현대중공업</span> : H-BlocNet, ProNet 시스템 및 실행 계획 시스템 개발
            <span className="tag">생산</span>
          </li>
          <li>
            <span className="bold">· SK IET</span> : SK IET 폴리이미드 MES 및 임가공 구축
            <br/><span className="tag">생산</span><span className="tag">물류</span><span className="tag">품질</span>
          </li>
          <li>
            <span className="bold">· SK IET</span> : 품질경영 시스템  구축
            <span className="tag">품질</span>
          </li>
        </ul>
      </div>
    </div>
    <div className="accomplishment-tab-content">
      <div className="accomplishment-tab-content-title">2018</div>
      <div className="accomplishment-tab-content-text">
        <ul>
          <li>
            <span className="bold">· SK IET</span> : 설비관리 시스템 구축
            <span className="tag">설비</span><span className="tag">품질</span>
          </li>
          <li>
            <span className="bold">· SK이노베이션</span> : SKI 충방전 설비관리 시스템 구축
            <span className="tag">설비</span>
          </li>
        </ul>
      </div>
    </div>
    <div className="accomplishment-tab-content">
      <div className="accomplishment-tab-content-title">2016</div>
      <div className="accomplishment-tab-content-text">
        <ul>
          <li>
            <span className="bold">· SK IET</span> : SK IET 글로벌 표준 IT 시스템 구축
            <br/><span className="tag">생산</span><span className="tag">설비</span><span className="tag">물류</span><span className="tag">품질</span>
          </li>
          <li>
            <span className="bold">· NEXFLEX</span> : 연성회로기판 MES 시스템 유지보수
            <br/><span className="tag">생산</span><span className="tag">물류</span><span className="tag">품질</span>
          </li>
        </ul>
      </div>
    </div>
    <div className="accomplishment-tab-content">
      <div className="accomplishment-tab-content-title">2015</div>
      <div className="accomplishment-tab-content-text">
        <ul>
          <li>
            <span className="bold">· 에센코어 반도체</span> : 에센코어 SCM 시스템 유지보수
            <br/><span className="tag">생산</span><span className="tag">물류</span>
          </li>
          <li>
            <span className="bold">· 에센코어 반도체</span> : 에센코어 반도체 공장 MES 구축
            <br/><span className="tag">생산</span><span className="tag">물류</span>
          </li>
          <li>
            배터리 화성 공장 MES 및 ECS 구축
            <br/><span className="tag">생산</span><span className="tag">설비</span><span className="tag">물류</span><span className="tag">품질</span>
          </li>
          <li>
            <span className="bold">· SK이노베이션</span> : 정보전자 소재 WMS 시스템 구축
            <br/><span className="tag">생산</span><span className="tag">물류</span>
          </li>
          <li>
            <span className="bold">· 창신INC</span> : NIKE 공장 Global MES 1차 구축
            <br/><span className="tag">생산</span><span className="tag">물류</span>
          </li>
          <li>
            <span className="bold">· 창신INC</span> : NIKE 공장 Global MES 표준화 및 Roadmap
            <br/><span className="tag">생산</span><span className="tag">물류</span>
          </li>
        </ul>
      </div>
    </div>
  </div>
)

//품질
const TabContent4 = () => (
  <div className="accomplishment-tab-content-container">
    <div className="accomplishment-tab-content">
      <div className="accomplishment-tab-content-title">2020</div>
      <div className="accomplishment-tab-content-text">
        <ul>
          <li>
            <span className="bold">· 롯데정밀화학</span> : 인천품질관리시스템
            업그레이드
          </li>
        </ul>
      </div>
    </div>
    <div className="accomplishment-tab-content">
      <div className="accomplishment-tab-content-title">2019</div>
      <div className="accomplishment-tab-content-text">
        <ul>
          <li>
            <span className="bold">· SK IET</span> : FCW 공장 생산관리 및 외주
            임가공 시스템 구축
          </li>
          <li>
            <span className="bold">· SK IET</span> : 품질경영 시스템구축
          </li>
          <li>
            <span className="bold">· SK이노베이션</span> : 충방전 설비관리
            시스템 구축
          </li>
          <li>
            <span className="bold">· SK IET</span> : 글로벌 표준 IT 시스템 구축
          </li>
          <li>
            <span className="bold">· NEXFLEX</span> : 연성회로기판 MES 시스템
            유지보수
          </li>
          <li>
            <span className="bold">· LG화학</span> : 차세대 생산/품질관리 시스템
            구축
          </li>
        </ul>
      </div>
    </div>
    <div className="accomplishment-tab-content">
      <div className="accomplishment-tab-content-title">2018</div>
      <div className="accomplishment-tab-content-text">
        <ul>
          <li>
            <span className="bold">· LG화학</span> : 차세대 생산관리 시스템
            PI컨설팅
          </li>
          <li>
            <span className="bold">· SK이노베이션</span> : 차세대 OIS 생산관리
            시스템 구축
          </li>
        </ul>
      </div>
    </div>
    <div className="accomplishment-tab-content">
      <div className="accomplishment-tab-content-title">2017</div>
      <div className="accomplishment-tab-content-text">
        <ul>
          <li>
            <span className="bold">· SK이노베이션</span> : 배터리 화성 공장 MES
            및 ECS 구축
          </li>
          <li>
            <span className="bold">· SK이노베이션</span> : 차세대 OIS 생산관리
            시스템 To-Be 설계
          </li>
          <li>
            <span className="bold">· CJ바이오</span> : Global MES 구축
          </li>
        </ul>
      </div>
    </div>
    <div className="accomplishment-tab-content"></div>
  </div>
)

const SolutionsTabArea = () => {
  const [selectedTab, setSelectedTab] = useState(1)

  return (
    <>
      <section className="accomplishment-tab-section">
        <h3 className="accomplishment-tab-section-title">수행실적</h3>
        <div className="accomplishment-tab-box-bg">
          <div className="accomplishment-tab-box-no-bg">
            <div className="accomplishment-tab-box-inside">
              <div className="accomplishment-tab-labels-wrapper">
                <div
                  className="accomplishment-tab-label"
                  style={labelStyle(selectedTab === 1)}
                  onClick={() => setSelectedTab(1)}
                >
                  <div>DT</div>
                </div>
                <div
                  className="accomplishment-tab-label"
                  style={labelStyle(selectedTab === 2)}
                  onClick={() => setSelectedTab(2)}
                >
                  <div>석유화학</div>
                </div>
                <div
                  className="accomplishment-tab-label"
                  style={labelStyle(selectedTab === 3)}
                  onClick={() => setSelectedTab(3)}
                >
                  <div>반도체 및 일반 제조</div>
                </div>
              </div>

              {selectedTab === 1 ? <TabContent1 /> : null}
              {selectedTab === 2 ? <TabContent2 /> : null}
              {selectedTab === 3 ? <TabContent3 /> : null}
            </div>
          </div>
        </div>
      </section>
      <section className="m_accomplishment-tab-section">
        <h3 className="accomplishment-tab-section-title">수행실적</h3>
        <div className="m_accomplishment-sub-section">
          <h3 className="m_accomplishment-sub-title">생산</h3>
          <h3 className="m_accomplishment-year">2019</h3>
          <p className="m_accomplishment-content">
            · SK IET: FCW 공장 생산관리 및 외주 임가공 시스템 구축
            <br />
            · SK IET: 글로벌 표준 IT 시스템 구축
            <br />
            · NEXFLEX: 연성회로기판 MES 시스템 유지보수
            <br />
            · 에센코어 반도체: SCM 시스템 유지보수
            <br />
            · SK이노베이션: 차세대 Global SCM PI 컨설팅
            <br />· LG화학: 차세대 생산/품질관리 시스템 구축
          </p>
          <h3 className="m_accomplishment-year">2018</h3>
          <p className="m_accomplishment-content">
            · 에센코어 반도체: 공장 Smart Factory MES 구축
            <br />
            · LG화학: 차세대 생산관리 시스템 PI컨설팅
            <br />
            · SK이노베이션: Digital Transformation 시스템 구축
            <br />· SK이노베이션: 차세대 OIS 생산관리 시스템 구축
          </p>
          <h3 className="m_accomplishment-year">2017</h3>
          <p className="m_accomplishment-content">
            · SK이노베이션: 배터리 화성 공장 MES 및 ECS 구축
            <br />
            · SK이노베이션: 차세대 OIS 생산관리 시스템 To-Be 설계
            <br />· CJ바이오: Global MES 구축
          </p>
          <h3 className="m_accomplishment-year">2015~2016</h3>
          <p className="m_accomplishment-content">
            · 창신INC: NIKE 공장 Global MES 1차 구축
            <br />· 창신INC: NIKE 공장 Global MES 표준화 및 Roadmap PI
          </p>
        </div>
        <div className="m_accomplishment-sub-section">
          <h3 className="m_accomplishment-sub-title">설비</h3>
          <h3 className="m_accomplishment-year">2019</h3>
          <p className="m_accomplishment-content">
            · SK이노베이션: 차세대 설비관리 시스템 구축
            <br />
            · SK IET: 설비관리 시스템 구축
            <br />
            · SK이노베이션: 충방전 설비관리 시스템 구축
            <br />· SK IET: 글로벌 표준 IT 시스템 구축
          </p>
          <h3 className="m_accomplishment-year">2017</h3>
          <p className="m_accomplishment-content">
            · SK이노베이션: 배터리 화성 공장 MES 및 ECS 구축
          </p>
        </div>
        <div className="m_accomplishment-sub-section">
          <h3 className="m_accomplishment-sub-title">물류</h3>
          <h3 className="m_accomplishment-year">2021</h3>
          <p className="m_accomplishment-content">· 금호석유화학: WMS 구축</p>
          <h3 className="m_accomplishment-year">2019</h3>
          <p className="m_accomplishment-content">
            · SK IET: FCW 공장 생산관리 및 외주 임가공 시스템 구축
            <br />
            · SK IET: 글로벌 표준 IT 시스템 구축
            <br />
            · NEXFLEX: 연성회로기판 MES 시스템 유지보수
            <br />· 에센코어 반도체: SCM 시스템 유지보수
          </p>
          <h3 className="m_accomplishment-year">2018</h3>
          <p className="m_accomplishment-content">
            · 에센코어 반도체: 공장 Smart Factory MES 구축
            <br />· LG화학: 차세대 생산관리 시스템 PI컨설팅
          </p>
          <h3 className="m_accomplishment-year">2017</h3>
          <p className="m_accomplishment-content">
            · SK이노베이션: 배터리 화성 공장 MES 및 ECS 구축
            <br />
            · SK이노베이션: 정보전자 소재 WMS 시스템 구축
            <br />· CJ바이오: Global MES 구축
          </p>
          <h3 className="m_accomplishment-year">2015~2016</h3>
          <p className="m_accomplishment-content">
            · 창신INC: NIKE 공장 Global MES 1차 구축
            <br />· 창신INC: NIKE 공장 Global MES 표준화 및 Roadmap PI
          </p>
        </div>
        <div className="m_accomplishment-sub-section">
          <h3 className="m_accomplishment-sub-title">품질</h3>
          <h3 className="m_accomplishment-year">2019</h3>
          <p className="m_accomplishment-content">
            · SK IET: FCW 공장 생산관리 및 외주 임가공 시스템 구축
            <br />
            · SK IET: 품질경영 시스템구축
            <br />
            · SK이노베이션: 충방전 설비관리 시스템 구축
            <br />
            · SK IET: 글로벌 표준 IT 시스템 구축
            <br />
            · NEXFLEX: 연성회로기판 MES 시스템 유지보수
            <br />· LG화학: 차세대 생산/품질관리 시스템 구축
          </p>
          <h3 className="m_accomplishment-year">2018</h3>
          <p className="m_accomplishment-content">
            · LG화학: 차세대 생산관리 시스템 PI컨설팅
            <br />· SK이노베이션: 차세대 OIS 생산관리 시스템 구축
          </p>
          <h3 className="m_accomplishment-year">2017</h3>
          <p className="m_accomplishment-content">
            · SK이노베이션: 배터리 화성 공장 MES 및 ECS 구축
            <br />
            · SK이노베이션: 차세대 OIS 생산관리 시스템 To-Be 설계
            <br />· CJ바이오: Global MES 구축
          </p>
        </div>
      </section>
    </>
  )
}

export default SolutionsTabArea
