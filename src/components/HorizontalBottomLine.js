import React from "react"
import w_square from "../images/w_square.jpg"

import "./horizontalBottomLine.css"

const HorizontalBottomLine = () => (
  <div className="horizontal-bottom-line">
    <img alt="가로선" src={w_square} />
  </div>
)

export default HorizontalBottomLine
