import React from "react"

import "./topImageSection.css"

const sectionStyle = img => ({
  background: `url("${img}") no-repeat center center`,
  backgroundSize: "cover",
})

const TopImageSection = ({ backgroundImage, title, description }) => (
  <div className="top-image-container" style={sectionStyle(backgroundImage)}>
    <div className="top-image-section-box">
      <h2>{title}</h2>
      <p>{description}</p>
    </div>
  </div>
)

export default TopImageSection
